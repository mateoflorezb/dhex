INCLUDE = -I"C:\SDL2x32\include" \
	-I"C:\SDL2x32\SDL2_mixer\include\SDL2" \
	-I"C:\SDL2x32\SDL2_net\include\SDL2"

LINK_LIB = -L"C:\SDL2x32\lib" \
	-L"C:\SDL2x32\SDL2_mixer\lib" \
	-L"C:\SDL2x32\SDL2_net\lib"

LINK_FLAGS = -lmingw32 -lSDL2main -lSDL2 -lSDL2_mixer -lSDL2_net -lopengl32

SOURCE = clist.c dhex_draw.c dhex_font.c dhex_bxg.c dhex_window_app.c dhex_rollback.c stbi.c resource_manager.c

CC = gcc

GAME_SOURCE = game_src/evilryu.c game_src/object.c game_src/global.c game_src/main.c game_src/basechar.c \
	game_src/hboxes.c

main : 
	$(CC) test.c $(SOURCE) $(INCLUDE) $(LINK_LIB) $(LINK_FLAGS) -o dh.exe -O3
	
game :
	$(CC) $(GAME_SOURCE) $(SOURCE) $(INCLUDE) $(LINK_LIB) $(LINK_FLAGS) -o DynamicHitlineEX.exe -O3
	
palgen:
	$(CC) palgen.c $(SOURCE) $(INCLUDE) $(LINK_LIB) $(LINK_FLAGS) -o palgen.exe -O3
	
boxgen:
	$(CC) boxgen.c $(SOURCE) $(INCLUDE) $(LINK_LIB) $(LINK_FLAGS) -o boxgen.exe -O3
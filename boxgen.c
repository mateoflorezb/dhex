/*
	BOXGEN - hit/hurtbox generator for Dynamic Hitline
	
	Usage:
	
	boxgen -new <plt>
		create a new bxg. Provide a palgen v2 palette file as <plt>
		
	boxgen -edit <bxg> <plt>
		edit an already existing bxg
*/

#include "dhex_window_app.h"
#include "dhex_font.h"
#include "dhex_bxg.h"
#include "resource_manager.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600

//24 fps sad 
#define MICROS_PER_FRAME (1000000/24)

#define SMALL_BUTTON_W 32
#define SMALL_BUTTON_H 24

#define MAX_SCALE 7.0

static void notice_usage() {
	printf(
	"BOXGEN - hit/hurtbox generator for Dynamic Hitline\n"
	"\n"
	"Usage: \n"
	"\n"
	"boxgen -new <plt>\n"
	"\tcreate a new bxg. Provide a palgen v2 palette file as <plt>\n"
	"\n"
	"boxgen -edit <bxg> <plt>\n"
	"\tedit an already existing bxg"
	"\n"
	"\n"
	);
	
}

static inline int iclamp(int val, int min, int max) {
	if (val < min) return min;
	if (val > max) return max;
	return val;
}

static inline int mouse_in(dhex_app* app, int x, int y, int w, int h) {
	int mx = app->input.mouse_x;
	int my = app->input.mouse_y;
	
	if (
		mx >= x && mx <= x+w &&
		my >= y && my <= y+h	
	) {
		return 1;
	}
	
	return 0;
}

static inline void _local_draw_hbox(dhex_rgba color, dhex_canvas* cv, int x, int y, int w, int h) {
	//color.a = 100;
	//dhex_set_colormode(DHEX_COLORMODE_BLEND, cv);
	//dhex_draw_rect( color, cv, x, y, w, h);
	color.a = 255;
	dhex_set_colormode(DHEX_COLORMODE_SET, cv);
	dhex_draw_rect( color, cv, x, y, w, 1);
	dhex_draw_rect( color, cv, x, y, 1, h);
	dhex_draw_rect( color, cv, x+w, y, 1, h);
	dhex_draw_rect( color, cv, x, y+h, w, 1);
}

static void* _local_dhex_load_spr(char* filename) {
	return dhex_load_sprite_png(filename);
}

static void _local_dhex_free_spr(void* resource) {
	dhex_free_sprite(resource);
}

//we need a list of dhex_sprite*, thus it's a dhex_sprite**,
//but we want to modify by reference, so we send a dhex_sprite***
static void _local_refresh_sprite_list(dhex_sprite*** spr_list, dhex_bxg* bxg, resource_manager* sprite_manager) {
	(*spr_list) = realloc( (*spr_list), bxg->nframes * sizeof(dhex_sprite*));
	for (int k = 0; k < bxg->nframes; k++) {
		(*spr_list)[k] = (dhex_sprite*)resource_manager_get(sprite_manager, bxg->frames[k].filename);
	}
}

//should have done it like this for every button tbh
//returns 1 if the button was clicked
int BUTTON(dhex_app* app, int xCenter, int yCenter, int bw, int bh, const char* caption, dhex_palette* pal) {
	dhex_draw_rect( (dhex_rgba){0,0,0,255}, app->main_canvas, xCenter - bw/2, yCenter - bh/2, bw, bh);
	
	dhex_font_draw_text(&DHEX_FONT_DEFAULT, pal, app->main_canvas, caption, DHEX_FONT_ALIGN_CENTERED,
	xCenter, yCenter, 2.0, 2.0);
	
	if ( mouse_in(app, xCenter - bw/2, yCenter - bh/2, bw, bh) ) {
		if (app->input.mouse_pressed) {
			return 1;
		}				
	}
	
	return 0;
}

typedef struct {
	int x,y;
} intpair;

int main(int argc, char* argv[]) {
	
	if (argc < 3) {
		notice_usage();
		return 0;
	}
	
	{
		char good = 0;
		
		if (argc == 3 && strcmp(argv[1], "-new")  == 0) {
			good = 1;
		}
		
		if (argc == 4 && strcmp(argv[1], "-edit") == 0) {
			good = 1;
		}		
		
		if (!good) {
			notice_usage();
			return 0;
		}
	}
	
	resource_manager* sprite_manager = resource_manager_create(_local_dhex_load_spr, _local_dhex_free_spr);
	
	dhex_font_init();
	dhex_palette pal_txt;
	pal_txt.ncolors = 1;
	pal_txt.colors[0] = (dhex_rgba){0,0,0,0};
	pal_txt.colors[1] = (dhex_rgba){0xFF,0xFF,0xFF,0xFF};
	
	dhex_palette pal_txt_red;
	pal_txt_red.ncolors = 1;
	pal_txt_red.colors[0] = (dhex_rgba){0,0,0,0};
	pal_txt_red.colors[1] = (dhex_rgba){0xFF,0x00,0x00,0xFF};
	
	dhex_bxg* bxg;
	
	if (argc == 3) {
		bxg = dhex_create_empty_bxg();
	}
	else {
		bxg = dhex_load_bxg(argv[2]);
		if (bxg == NULL) {
			printf("Error: failed to load %s\n", argv[2]);
			return 1;
		}
	}
	
	dhex_palette* pal;
	
	{
		int palpos = 2;
		if (argc == 4) palpos = 3;
		
		pal = dhex_load_palette(argv[palpos]);
		if (pal == NULL) {
			printf("Error: failed to load %s\n", argv[palpos]);
			return 1;				
		}
		
	}
	
	dhex_app* app = dhex_create_app("BOXGEN V2", SCREEN_WIDTH, SCREEN_HEIGHT, (double)SCREEN_WIDTH / (double)SCREEN_HEIGHT, 1, 0);
	
	dhex_sprite** spr_list = NULL;
	
	int background_brightness = 0;
	int current_frame_index = 0;
	char stdin_scan_queued = 0;
	char filename_input[90] = "";
	int category = 0; //0 for hurtboxes, 1 for hitboxes 
	int current_box_id[2]; //access with [category]
	current_box_id[0] = 0;
	current_box_id[1] = 0;
	float scale = 2.0; //for the sprite 
	char flicker = 0;
	int export_bxg_button_shine_left = 0;
	
	//basically sprite offset but as a percentage
	double spr_visual_x_ptg = 0.5;
	double spr_visual_y_ptg = 0.5;
	
	//will only use the hit/hurt box members
	dhex_bxg_frame copied_boxes;
	char valid_copy = 0; //set to 1 if the data in copied_boxes is not garbage
	
	_local_refresh_sprite_list(&spr_list, bxg, sprite_manager);
	
	while ( app->quit == 0 ) {
		int64_t tStart = dhex_get_micros();
		dhex_refresh_app(app);
		flicker = ~flicker;
		
		if (export_bxg_button_shine_left > 0) export_bxg_button_shine_left--; 
		
		if (stdin_scan_queued) {
			printf("\n(boxgen) enter filename for new frame's sprite\n");
			scanf("%85s", filename_input);
			
			dhex_bxg_frame new_frame;
			int len = strlen(filename_input) + 1;
			new_frame.filename = malloc(len);
			strcpy(new_frame.filename, filename_input);
			new_frame.loops = 0;
			new_frame.xcenter = 0;
			new_frame.ycenter = 0;
			
			for (int ccat = 0; ccat <= 1; ccat++)
			for (int ii = 0; ii < DHEX_BXG_MAX_BOXES; ii++) {
				new_frame.boxes[ccat][ii].x = 0;
				new_frame.boxes[ccat][ii].y = 0;
				new_frame.boxes[ccat][ii].w = 0;
				new_frame.boxes[ccat][ii].h = 0;
			}
			
			dhex_bxg_insert_frame(bxg, new_frame, current_frame_index);
			
			free(new_frame.filename);
			
			_local_refresh_sprite_list(&spr_list, bxg, sprite_manager);
			stdin_scan_queued = 0;
		}
		
		// background color ///////////////////////
		u8 br = (u8)background_brightness;
		dhex_draw_rect( (dhex_rgba){br,br,br,255}, app->main_canvas, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
		
		// this will be used in the grey panels as well 
		int panel_w = 320;
		int panel_h = 90;
		
		// sprite /////////////////////////////////
		
		{
			int xCenter = panel_w + (SCREEN_WIDTH  - panel_w)/2;
			int yCenter = panel_h + (SCREEN_HEIGHT - panel_h)/2;
			if (current_frame_index == bxg->nframes) {
				int ww = 190;
				dhex_draw_rect( (dhex_rgba){0,0,0,255}, app->main_canvas, xCenter - ww/2, yCenter - SMALL_BUTTON_H/2, ww, SMALL_BUTTON_H);
				dhex_font_draw_text(&DHEX_FONT_DEFAULT, &pal_txt, app->main_canvas, "no sprite", DHEX_FONT_ALIGN_CENTERED,
				xCenter, yCenter, 2.0, 2.0);				
			}
			else {
				dhex_sprite* spr = spr_list[current_frame_index];
				if (spr != NULL) {
					
					int spr_xoffset = spr->w * spr_visual_x_ptg;
					int spr_yoffset = spr->h * spr_visual_y_ptg;
					
					dhex_draw_sprite_ex(spr, pal, app->main_canvas, xCenter, yCenter,
						spr_xoffset, spr_yoffset, scale, scale);
						
					//calculate spr top left corner coords 
					int spr_xcorner = xCenter - (spr_xoffset) * scale;
					int spr_ycorner = yCenter - (spr_yoffset) * scale;
					
					//calculate center for the cross that marks sprite offset 
					int xcross = spr_xcorner + bxg->frames[current_frame_index].xcenter * scale;
					int ycross = spr_ycorner + bxg->frames[current_frame_index].ycenter * scale;
					
					
					//draw hit/hurtboxes
					for (int ccat = 0; ccat <= 1; ccat++)
					for (int bx = 0; bx < DHEX_BXG_MAX_BOXES; bx++) {
						
						dhex_rgba color = (dhex_rgba){0,50,220,255}; //blue hurtboxes 
						if (ccat == 1) color = (dhex_rgba){255,0,0,255}; //red hitboxes 
						
						//draw every other frame the currently edited box, and always draw the other boxes
						if (ccat == category && bx == current_box_id[category]) {
							color = (dhex_rgba){255,0,255,255}; 
						}
					
						dhex_box* bref = &bxg->frames[current_frame_index].boxes[ccat][bx];
						if (bref->w > 0) {
							int xx = xcross + bref->x * scale;
							int yy = ycross + bref->y * scale;
							int ww = bref->w * scale;
							int hh = bref->h * scale;
							_local_draw_hbox(color, app->main_canvas, xx, yy, ww, hh);
							
						}
					}
					
					int cross_size = 25;
					
					//first draw a black cross at (x+1,y+1) then draw a white cross at (x,y)
					//done for visibility reasons
					for (int cs = 0; cs < 2; cs++) {
						dhex_rgba color = (dhex_rgba){0,0,0,255};
						int ofs = 1;
						if (cs > 0) {
							color = (dhex_rgba){255,255,255,255};
							ofs = 0;
						}
						dhex_draw_rect( color, app->main_canvas, ofs+xcross - cross_size, ofs+ycross, cross_size*2, 1);
						dhex_draw_rect( color, app->main_canvas, ofs+xcross, ofs+ycross - cross_size, 1, cross_size*2);
					}
				}
			} 
		}
		
		// background color slider ///////////////////////
		for (int k = 0; k < 2; k++)
		{
			int xCenter = panel_w + 20 + (SMALL_BUTTON_W + 4)*k;
			int yCenter = panel_h + 20;
			
			const char* s1 = "%l";
			const char* s2 = "%r";
			const char* s = s1;
			if (k == 1) s = s2;
			
			int bw = SMALL_BUTTON_W;
			int bh = SMALL_BUTTON_H;
			
			dhex_draw_rect( (dhex_rgba){0,0,0,0xFF}, app->main_canvas, xCenter - bw/2, yCenter - bh/2, bw, bh );
			dhex_font_draw_text(&DHEX_FONT_DEFAULT, &pal_txt, app->main_canvas, s, DHEX_FONT_ALIGN_CENTERED,
			xCenter, yCenter, 2.0, 2.0);	
			
			if (mouse_in(app, xCenter - bw/2, yCenter - bh/2, bw, bh ) ) {
				if (app->input.mouse_held) {
					int dir = -4;
					if (k == 1) dir *= -1;
					background_brightness = iclamp(background_brightness + dir, 0, 255);
				}
			}
		}
		
		// sprite visualization center slider //////////////////
		
		{
			int size = 85;
			
			int xCenter = SCREEN_WIDTH  - (size/2 + 10);
			int yCenter = SCREEN_HEIGHT - (size/2 + 10);
			
			dhex_draw_rect( (dhex_rgba){0,0,0,0xFF}, app->main_canvas, 0+xCenter - size/2, 0+yCenter - size/2, size-0, size-0);
			dhex_draw_rect( (dhex_rgba){0xFF,0xFF,0xFF,0xFF}, app->main_canvas, 1+xCenter - size/2, 1+yCenter - size/2, size-2, size-2);
			
			if (mouse_in(app, +xCenter - size/2, 0+yCenter - size/2, size-0, size-0) ) {
				if (app->input.mouse_held) {
					int xx = (app->input.mouse_x) - (xCenter - size/2);
					int yy = (app->input.mouse_y) - (yCenter - size/2);
					spr_visual_x_ptg = (double)xx / (double)size;
					spr_visual_y_ptg = (double)yy / (double)size;
				}
			}
		}
		
		// scale slider ///////////////////////
		for (int k = 0; k < 2; k++)
		{
			int xCenter = SCREEN_WIDTH  - 120;
			int yCenter = SCREEN_HEIGHT - 50 + (SMALL_BUTTON_H + 4)*k;
			
			const char* s1 = "%u";
			const char* s2 = "%d";
			const char* s = s1;
			if (k == 1) s = s2;
			
			int bw = SMALL_BUTTON_W;
			int bh = SMALL_BUTTON_H;
			
			dhex_draw_rect( (dhex_rgba){0,0,0,0xFF}, app->main_canvas, xCenter - bw/2, yCenter - bh/2, bw, bh );
			
			dhex_font_draw_text(&DHEX_FONT_DEFAULT, &pal_txt, app->main_canvas, s, DHEX_FONT_ALIGN_CENTERED,
			xCenter, yCenter, 2.0, 2.0);	
			
			if (mouse_in(app, xCenter - bw/2, yCenter - bh/2, bw, bh ) ) {
				if (app->input.mouse_held) {
					float dir = 0.1;
					if (k == 1) dir *= -1.0;
					scale += dir;
					if (scale < 1.0) scale = 1.0;
					if (scale > MAX_SCALE) scale = MAX_SCALE;
				}
			}
		}
		
		//grey panels /////////////////////////////
		
		u8 gpcolor = 128;
		dhex_draw_rect( (dhex_rgba){gpcolor,gpcolor,gpcolor,255}, app->main_canvas, 0, 0, SCREEN_WIDTH, panel_h);
		dhex_draw_rect( (dhex_rgba){gpcolor,gpcolor,gpcolor,255}, app->main_canvas, 0, panel_h, panel_w, SCREEN_HEIGHT);
		
		// frame index stuff ///////////////////////
		{
			int xCenter, yCenter;
			xCenter = 100+SCREEN_WIDTH/2;
			yCenter = 20;
			
			char str[16];
			if (bxg->nframes != 0)
			sprintf(str, "%d of %d\0", current_frame_index + 1, bxg->nframes);
			else
			strcpy(str, "no frames");
		
			dhex_font_draw_text(&DHEX_FONT_DEFAULT, &pal_txt, app->main_canvas, str, DHEX_FONT_ALIGN_CENTERED,
			xCenter, yCenter, 2.0, 2.0);
			
			for (int k = 0; k < 2; k++) { //change frame index buttons
				int offset = 150;
				int xx;
				char* txt1 = "%l";
				char* txt2 = "%r";
				char* tx = txt1;
				
				xx = xCenter - offset;
				if (k == 1) {
					xx = xCenter + offset;
					tx = txt2;
				}
				dhex_draw_rect( (dhex_rgba){0,0,0,255}, app->main_canvas, xx - SMALL_BUTTON_W/2,
					yCenter - SMALL_BUTTON_H/2, SMALL_BUTTON_W, SMALL_BUTTON_H);
					
				dhex_font_draw_text(&DHEX_FONT_DEFAULT, &pal_txt, app->main_canvas, tx, DHEX_FONT_ALIGN_CENTERED,
					xx, yCenter, 2.0, 2.0);		

				if (mouse_in(app, xx - SMALL_BUTTON_W/2, yCenter - SMALL_BUTTON_H/2, SMALL_BUTTON_W, SMALL_BUTTON_H) ) {
					if ( app->input.mouse_pressed ) {
						int inc = -1;
						if (k == 1) inc = 1;
						
						current_frame_index = iclamp(current_frame_index + inc, 0, bxg->nframes);
					}
				}
			
			}
			
			yCenter = 60;
			int bw, bh;
			bw = 120;
			bh = 24;
			
			dhex_draw_rect( (dhex_rgba){0,0,0,255}, app->main_canvas, xCenter - bw/2, yCenter - bh/2, bw, bh);
			dhex_font_draw_text(&DHEX_FONT_DEFAULT, &pal_txt, app->main_canvas, "insert", DHEX_FONT_ALIGN_CENTERED,
			xCenter, yCenter, 2.0, 2.0);
			
			if (mouse_in(app, xCenter - bw/2, yCenter - bh/2, bw, bh) ) {
				if ( app->input.mouse_pressed ) {
					stdin_scan_queued = 1;
					dhex_set_colormode(DHEX_COLORMODE_BLEND, app->main_canvas);
					dhex_set_layer(DHEX_LAYER_1, app->main_canvas);
					dhex_draw_rect( (dhex_rgba){0,0,0,167}, app->main_canvas, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
					dhex_set_colormode(DHEX_COLORMODE_SET, app->main_canvas);
					dhex_draw_rect( (dhex_rgba){0,0,0,255}, app->main_canvas, 0, SCREEN_HEIGHT/2 - 15, SCREEN_WIDTH, 30);
					dhex_font_draw_text(&DHEX_FONT_DEFAULT, &pal_txt, app->main_canvas, "waiting for command line input", DHEX_FONT_ALIGN_CENTERED,
					SCREEN_WIDTH/2, SCREEN_HEIGHT/2, 2.0, 2.0);
					
					dhex_set_layer(DHEX_LAYER_0, app->main_canvas);
				}
			}
		}
		
		// delete frame button /////////////////////////
		
		{
			int xCenter = SCREEN_WIDTH - 100;
			int yCenter = 60;
			int bw, bh;
			bw = 140;
			bh = 24;
			
			dhex_draw_rect( (dhex_rgba){0,0,0,255}, app->main_canvas, xCenter - bw/2, yCenter - bh/2, bw, bh);
			dhex_font_draw_text(&DHEX_FONT_DEFAULT, &pal_txt, app->main_canvas, "delete", DHEX_FONT_ALIGN_CENTERED,
			xCenter, yCenter, 2.0, 2.0);
			
			if (current_frame_index < bxg->nframes)
			if (mouse_in(app, xCenter - bw/2, yCenter - bh/2, bw, bh) ) {
				if ( app->input.mouse_pressed ) {
					dhex_bxg_delete_frame(bxg, current_frame_index); 
					_local_refresh_sprite_list(&spr_list, bxg, sprite_manager);
				}
			}
			
		}
		
		// switch category ///////////////////////////
		
		{
			int xCenter = 100;
			int yCenter = 60;
			int bw, bh;
			bw = 140;
			bh = 24;
			
			dhex_draw_rect( (dhex_rgba){0,0,0,255}, app->main_canvas, xCenter - bw/2, yCenter - bh/2, bw, bh);
			dhex_font_draw_text(&DHEX_FONT_DEFAULT, &pal_txt, app->main_canvas, "switch", DHEX_FONT_ALIGN_CENTERED,
			xCenter, yCenter, 2.0, 2.0);
			
			if (mouse_in(app, xCenter - bw/2, yCenter - bh/2, bw, bh) ) {
				if ( app->input.mouse_pressed ) {
					if (category == 0) category = 1;
					else category = 0;
				}
			}

			char* cat0 = "hurtboxes";
			char* cat1 = "hitboxes";
			char* cat_ = cat0;
			if (category == 1) cat_ = cat1;
			
			dhex_font_draw_text(&DHEX_FONT_DEFAULT, &pal_txt, app->main_canvas, cat_, DHEX_FONT_ALIGN_CENTERED,
			xCenter, yCenter - 30, 2.0, 2.0);			
			
		}
		
		// select box index //////////////////////////
		
		{
			int xCenter = 20;
			int yCenter = 90;
			
			char str[20];
			sprintf(str, "box %d of %d\0", 1+current_box_id[category], DHEX_BXG_MAX_BOXES);
			
			dhex_font_draw_text(&DHEX_FONT_DEFAULT, &pal_txt, app->main_canvas, str, DHEX_FONT_ALIGN_LEFT,
			xCenter, yCenter, 2.0, 2.0);	

			xCenter += 70;
			yCenter += 36;
			
			//select box index buttons 
			for (int ii = 0; ii < 2; ii++) {
				if ( (ii == 0 && current_box_id[category] > 0) || (ii == 1 && current_box_id[category] < DHEX_BXG_MAX_BOXES - 1) ) {
					int dir = -1;
					strcpy(str, "%l");
					if (ii == 1) {
						dir *= -1;
						strcpy(str, "%r");
					}
					
					int offset = 20 * dir;
					
					int bw = SMALL_BUTTON_W;
					int bh = SMALL_BUTTON_H;
					
					dhex_draw_rect( (dhex_rgba){0,0,0,255}, app->main_canvas, offset+xCenter - bw/2, yCenter - bh/2, bw, bh);
					dhex_font_draw_text(&DHEX_FONT_DEFAULT, &pal_txt, app->main_canvas, str, DHEX_FONT_ALIGN_CENTERED,
					offset+xCenter, yCenter, 2.0, 2.0);
					
					if (mouse_in(app, offset + xCenter - bw/2, yCenter - bh/2, bw, bh) ) {
						if ( app->input.mouse_pressed ) {
							current_box_id[category] = iclamp(current_box_id[category] + dir, 0, DHEX_BXG_MAX_BOXES - 1);
						}
					}
				}
			}
		}
		
		// current box propeties ////////////////////////
		
		{
			int xCenter = 20;
			int yCenter = 150;			
			char str[16];
			
			if (current_frame_index != bxg->nframes)
			for (int yy = 0; yy < 2; yy++)
			for (int xx = 0; xx < 2; xx++) {
				int xc = xCenter + 150*xx;
				int yc = yCenter + 30*yy;
				
				if (yy == 0) {
					if (xx == 0)
						sprintf(str, "x %d\0", bxg->frames[current_frame_index].boxes[category][current_box_id[category]].x);
					else 
						sprintf(str, "y %d\0", bxg->frames[current_frame_index].boxes[category][current_box_id[category]].y);
						
				}
				else {
					if (xx == 0)
						sprintf(str, "w %d\0", bxg->frames[current_frame_index].boxes[category][current_box_id[category]].w);
					else 
						sprintf(str, "h %d\0", bxg->frames[current_frame_index].boxes[category][current_box_id[category]].h);					
				}
			
				dhex_font_draw_text(&DHEX_FONT_DEFAULT, &pal_txt, app->main_canvas, str, DHEX_FONT_ALIGN_LEFT,
				xc, yc, 2.0, 2.0);
			}
			
		}
		
		// current box sliders //////////////////////////////////
		if (current_frame_index != bxg->nframes)
		{
			int xCenter = 30;
			int yCenter = 230;			
			char str[12];
			
			for (int k = 0; k < 4; k++) { //for each category (x,y,w,h)
				switch(k) {
					case 0: sprintf(str, "x\0"); break;
					case 1: sprintf(str, "y\0"); break;
					case 2: sprintf(str, "w\0"); break;
					case 3: sprintf(str, "h\0"); break;
				}
				
				int offset = 30;
				
				dhex_font_draw_text(&DHEX_FONT_DEFAULT, &pal_txt, app->main_canvas, str, DHEX_FONT_ALIGN_CENTERED,
				xCenter, yCenter + k*offset, 2.0, 2.0);			
				
				int32_t* target_value;
				switch(k) {
					case 0: target_value = &bxg->frames[current_frame_index].boxes[category][current_box_id[category]].x; break;
					case 1: target_value = &bxg->frames[current_frame_index].boxes[category][current_box_id[category]].y; break;
					case 2: target_value = &bxg->frames[current_frame_index].boxes[category][current_box_id[category]].w; break;
					case 3: target_value = &bxg->frames[current_frame_index].boxes[category][current_box_id[category]].h; break;
				}

				for (int btn = 0; btn < 4; btn++) { //for each slider button 
					switch(btn) {
						case 0: sprintf(str, "%%l\0"); break;
						case 1: sprintf(str, "%%r\0"); break;
						case 2: sprintf(str, "%%l%%l\0"); break;
						case 3: sprintf(str, "%%r%%r\0"); break;
					}
					
					int xc = 60+xCenter + btn*54;
					int yc = yCenter + k*offset;
					
					int bw = 46;
					int bh = SMALL_BUTTON_H;
					
					dhex_draw_rect( (dhex_rgba){0,0,0,255}, app->main_canvas, xc - bw/2, yc - bh/2, bw, bh);
					dhex_font_draw_text(&DHEX_FONT_DEFAULT, &pal_txt, app->main_canvas, str, DHEX_FONT_ALIGN_CENTERED,
					xc, yc, 2.0, 2.0);

					if ( mouse_in(app, xc - bw/2, yc - bh/2, bw, bh) ) {
						if ( (btn <= 1 && app->input.mouse_pressed) || (btn >= 2 && app->input.mouse_held) ) {
							int dir;
							if (btn == 0 || btn == 2) {
								dir = -1;
							}
							else {
								dir = 1;
							}
							
							if (btn >= 2) dir *= 3; //go faster in the held sliders 
							
							(*target_value) += dir;
						
							if (k >= 2) { //forbid w and h to go negative 
								(*target_value) = iclamp(*target_value, 0, 99999);
							}
						}
					}
				}
			}
		}
		
		// sprite origin sliders ///////////////////////////////////
		if (current_frame_index != bxg->nframes)
		{
			int xCenter = 20;
			int yCenter = 350;			
			char str[12];
			dhex_font_draw_text(&DHEX_FONT_DEFAULT, &pal_txt, app->main_canvas, "sprite center", DHEX_FONT_ALIGN_LEFT,
			xCenter, yCenter, 2.0, 2.0);
			yCenter += 40;	
			xCenter = 30;			
			
			//difference between relative and absolute is that relative won't move the boxes, only the center 
			for (int k = 0; k < 4; k++) { //for each category (x absolute, y absolute, x relative, y relative)
				switch(k) {
					case 0: sprintf(str, "xa\0"); break;
					case 1: sprintf(str, "ya\0"); break;
					case 2: sprintf(str, "xr\0"); break;
					case 3: sprintf(str, "yr\0"); break;
				}
			
				int offset = 30;
				
				dhex_font_draw_text(&DHEX_FONT_DEFAULT, &pal_txt, app->main_canvas, str, DHEX_FONT_ALIGN_CENTERED,
				xCenter, yCenter + k*offset, 2.0, 2.0);
				
				int16_t* target_value;
				
				
				if (k == 0 || k == 2) {
					target_value = &bxg->frames[current_frame_index].xcenter;
					//relative_target = &bxg->frames[current_frame_index].boxes[category][current_box_id[category]].x;
				}
				else {
					target_value = &bxg->frames[current_frame_index].ycenter;
					//relative_target = &bxg->frames[current_frame_index].boxes[category][current_box_id[category]].y;
				}
				
				for (int btn = 0; btn < 4; btn++) { //for each slider button 
					switch(btn) {
						case 0: sprintf(str, "%%l\0"); break;
						case 1: sprintf(str, "%%r\0"); break;
						case 2: sprintf(str, "%%l%%l\0"); break;
						case 3: sprintf(str, "%%r%%r\0"); break;
					}
				
					int xc = 60+xCenter + btn*54;
					int yc = yCenter + k*offset;
					
					int bw = 46;
					int bh = SMALL_BUTTON_H;
					
					dhex_draw_rect( (dhex_rgba){0,0,0,255}, app->main_canvas, xc - bw/2, yc - bh/2, bw, bh);
					dhex_font_draw_text(&DHEX_FONT_DEFAULT, &pal_txt, app->main_canvas, str, DHEX_FONT_ALIGN_CENTERED,
					xc, yc, 2.0, 2.0);
					
					if ( mouse_in(app, xc - bw/2, yc - bh/2, bw, bh) ) {
						if ( (btn <= 1 && app->input.mouse_pressed) || (btn >= 2 && app->input.mouse_held) ) {
							
							int dir;
							
							if (btn == 0 || btn == 2) {
								dir = -1;
							}
							else {
								dir = 1;			
							}
							
							if (btn >= 2) dir *= 3; //go faster in the held sliders 
							
							(*target_value) += dir;
						
							if (k >= 2) { //relative cases 
								
								for (int ccat = 0; ccat <= 1; ccat++) //for each category (hit / hurtbox)
								for (int bx = 0; bx < DHEX_BXG_MAX_BOXES; bx++) { //for each box 
									if (bxg->frames[current_frame_index].boxes[ccat][bx].w > 0) {
										
										int32_t* relative_target; //reference to the boxes' x or y pos depending on the case 
										if (k == 0 || k == 2) 
											relative_target = &bxg->frames[current_frame_index].boxes[ccat][bx].x;
										else 
											relative_target = &bxg->frames[current_frame_index].boxes[ccat][bx].y;
										
										(*relative_target) -= dir;
									}
								}
							}
						}
					}
				}
			}
		}
		
		// n loops sliders ///////////////////////////
		if (current_frame_index != bxg->nframes)
		{
			int xCenter = 20;
			int yCenter = 510;			
			char str[14];
			int16_t* loops = &bxg->frames[current_frame_index].loops;
			sprintf(str, "loops %d", *loops);
			dhex_font_draw_text(&DHEX_FONT_DEFAULT, &pal_txt, app->main_canvas, str, DHEX_FONT_ALIGN_LEFT,
			xCenter, yCenter, 2.0, 2.0);
			
			xCenter += 202;
			yCenter += 5;
			for (int bt = 0; bt < 2; bt++) { //for each button 
				int bw = SMALL_BUTTON_W;
				int bh = SMALL_BUTTON_H;
				int xc = xCenter + (bw + 4)*bt;
				
				if (bt == 0) sprintf(str,"%%l"); else sprintf(str,"%%r");
				
				dhex_draw_rect( (dhex_rgba){0,0,0,255}, app->main_canvas, xc - bw/2, yCenter - bh/2, bw, bh);
				
				dhex_font_draw_text(&DHEX_FONT_DEFAULT, &pal_txt, app->main_canvas, str, DHEX_FONT_ALIGN_CENTERED,
				xc, yCenter, 2.0, 2.0);		

				if ( mouse_in(app, xc - bw/2, yCenter - bh/2, bw, bh) ) {
					if (app->input.mouse_pressed) {
						int dir = -1;
						if (bt == 1) dir = 1;
						(*loops) = iclamp((*loops) + dir, 0, 9999);
					}
				}
			}
		}
		
		// export bxg button ///////////////////////////
		{
			int xCenter = 160;
			int yCenter = 560;			
			int bw = 220;
			int bh = SMALL_BUTTON_H;
			
			dhex_draw_rect( (dhex_rgba){0,0,0,255}, app->main_canvas, xCenter - bw/2, yCenter - bh/2, bw, bh);
			
			dhex_palette* _pal_t = &pal_txt;
			
			if (export_bxg_button_shine_left > 0) {
				_pal_t = &pal_txt_red;
			}
			
			dhex_font_draw_text(&DHEX_FONT_DEFAULT, _pal_t, app->main_canvas, "export bxg", DHEX_FONT_ALIGN_CENTERED,
			xCenter, yCenter, 2.0, 2.0);
			
			if ( mouse_in(app, xCenter - bw/2, yCenter - bh/2, bw, bh) ) {
				if (app->input.mouse_pressed) {
					
					export_bxg_button_shine_left = 17;
					
					if (argc == 3) { //aka new bxg
						 dhex_save_bxg(bxg, "new.bxg");
					}
					else {
						dhex_save_bxg(bxg, argv[2] );
					}
				}				
			}
		}
		
		//copy and paste boxes
		#define COPY_PASTE_X 410
		#define COPY_PASTE_Y 550
		if (BUTTON(app, COPY_PASTE_X, COPY_PASTE_Y, 120, SMALL_BUTTON_H, "COPY", &pal_txt)) {
			valid_copy = 1;
			for (int c = 0; c < 2; c++)  //category
			for (int b = 0; b < DHEX_BXG_MAX_BOXES; b++) //box
			{
				memcpy(&(copied_boxes.boxes[c][b]), &(bxg->frames[current_frame_index].boxes[c][b]), sizeof(dhex_box));
			}				
		}
		
		if (valid_copy) 
		if (BUTTON(app, COPY_PASTE_X, COPY_PASTE_Y + 5 + SMALL_BUTTON_H, 120, SMALL_BUTTON_H, "PASTE", &pal_txt)) {
			for (int c = 0; c < 2; c++)  //category
			for (int b = 0; b < DHEX_BXG_MAX_BOXES; b++) //box
			{
				memcpy(&(bxg->frames[current_frame_index].boxes[c][b]), &(copied_boxes.boxes[c][b]), sizeof(dhex_box));
			}
		}
		
		dhex_refresh_window(app);
		
		int64_t tEnd = dhex_get_micros();
		dhex_microsleep(iclamp(MICROS_PER_FRAME - (tEnd - tStart), 0, MICROS_PER_FRAME) );
	}
	
	return 0;
}
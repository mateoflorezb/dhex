#include "dhex_bxg.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

dhex_bxg* dhex_create_empty_bxg() {
	dhex_bxg* bxg = malloc(sizeof(dhex_bxg));
	bxg->nframes = 0;
	bxg->frames = NULL;
	return bxg;
}

void dhex_bxg_insert_frame(dhex_bxg* bxg, dhex_bxg_frame frame, int pos) {
	
	if (pos > bxg->nframes || pos < 0) return;
	
	bxg->frames = realloc(bxg->frames, (1 + bxg->nframes) * sizeof(dhex_bxg_frame));
	bxg->nframes += 1;
	
	//shift every index 
	for (int k = bxg->nframes - 1; k > pos; k--) {
		bxg->frames[k] = bxg->frames[k - 1];
	} 
	
	bxg->frames[pos] = frame;
	char* strCopy = malloc(1 + strlen(frame.filename));
	strcpy(strCopy, frame.filename);
	bxg->frames[pos].filename = strCopy;
}

int dhex_bxg_delete_frame(dhex_bxg* bxg, int pos) {
	
	if (pos >= bxg->nframes || pos < 0) return -1;
	
	free(bxg->frames[pos].filename);
	
	//shift every index 
	for (int k = pos; k < bxg->nframes; k++) {
		bxg->frames[k] = bxg->frames[k + 1];
	} 	
	
	bxg->frames = realloc(bxg->frames, (bxg->nframes - 1) * sizeof(dhex_bxg_frame));
	bxg->nframes -= 1;
}

void dhex_free_bxg(dhex_bxg* bxg) {
	
	if (bxg->frames != NULL) {
		
		for (int k = 0; k < bxg->nframes; k++) {
			free(bxg->frames[k].filename);
		}
		free(bxg->frames);
	}
	
	free(bxg);
	
}

void dhex_save_bxg(dhex_bxg* bxg, const char* filename) {
	
	FILE* f = fopen(filename, "wb");
	
	if (f) {
		fwrite(&bxg->nframes, sizeof(int16_t), 1, f);
		
		for (int k = 0; k < bxg->nframes; k++) {
			fprintf(f, "%s\n", bxg->frames[k].filename);
		}
		
		for (int k = 0; k < bxg->nframes; k++) {
			fwrite(&bxg->frames[k], sizeof(dhex_bxg_frame), 1, f);
		}
	}
	
	fclose(f);
}

dhex_bxg* dhex_load_bxg(const char* filename) {
	
	FILE* f = fopen(filename, "rb");
	
	dhex_bxg* bxg = NULL;
	
	if (f) {
		bxg = malloc(sizeof(dhex_bxg));
		fread(&bxg->nframes, sizeof(int16_t), 1, f);
		
		if (bxg->nframes <= 0) {
			free(bxg);
			return NULL;
		}
		
		bxg->frames = malloc(sizeof(dhex_bxg_frame) * bxg->nframes);
		char** strList = malloc(bxg->nframes * sizeof(char*));
		
		for (int k = 0; k < bxg->nframes; k++) {
			//stSize --> string size, how many bytes we need for the nth spr filename
			int stSize = 0;
			fpos_t start_pos;
			fgetpos(f, &start_pos);

			unsigned char c;
			do {
				stSize++;
				c = fgetc(f);
			} while(c != '\n');
			
			strList[k] = malloc(stSize);
			fsetpos(f, &start_pos);
			fgets(strList[k], stSize, f);
			
			//move 1 byte forward otherwise we are stuck in the \n terminator
			fseek(f, 1, SEEK_CUR);
		}
		
		//now strList has stored every filename by index 
		
		for (int k = 0; k < bxg->nframes; k++) {
			fread(&bxg->frames[k], sizeof(dhex_bxg_frame), 1, f);
			bxg->frames[k].filename = strList[k];
		}
		
		free(strList);		
	}
	
	fclose(f);	
	
	return bxg;
}
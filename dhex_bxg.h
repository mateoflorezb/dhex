/*
	dhex interface for loading/saving bxg files.
	bxg files define an animation (what frames and how many ticks must elapse to
		advance to the next frame, along with hit/hurtbox definitions for each frame).
	Since this isn't related to any rendering, this is a stand alone module.
	
	bxg file format:
	
	int16_t <n> that defines how many frames there are. Any number <= 0 will be interpreted as
	the file being malformed.
	
	<n> consecutive null terminated strings that indicate the filename of the nth sprite. Sprites are expected
	to be palettized PNGs resulting from palgen. The sprite file is expected to be in the same folder as
	the bxg file (so for Dynamic Hitline, a character's data will be dumped in a single folder)
	
	<n> dhex_bxg_frames, but instead of storing char* filenames, it will store an int that is to be interpreted 
	as an index 
*/

#ifndef _dhex_bxg_h
#define _dhex_bxg_h

#include <stdint.h>

//aka how many boxes are available to define the hurtbox
//this same number is how many boxes are available to define the hitbox
#define DHEX_BXG_MAX_BOXES 12

//(x,y) is relative to the origin of the character.
typedef struct {
	int32_t x,y,w,h;
} dhex_box;

typedef struct {
	char* filename; //malloc'd
	dhex_box boxes[2][DHEX_BXG_MAX_BOXES]; //0 for hurtbox, 1 for hitbox
	int16_t loops; //aka how many times should the nth animation be repeated
	int16_t xcenter, ycenter; //sprite center relative to top left corner
} dhex_bxg_frame;

//to get the nth frame of a bxg just access bxg->frames[n]
typedef struct {
	int16_t nframes;
	dhex_bxg_frame* frames; //malloc'd and realloc'd 
} dhex_bxg;

dhex_bxg* dhex_create_empty_bxg(void);
void      dhex_free_bxg(dhex_bxg* bxg);

//The new frame will be inserted before the frame that corresponds to index <pos>.
//Every index is adjusted accordingly.
//This means that for inserting a frame at the end you must specify <pos> as whatever
//the current number of frames is.
//The frame will make a copy of the char* filename provided as argument.
void dhex_bxg_insert_frame(dhex_bxg* bxg, dhex_bxg_frame frame, int pos);

//Delete frame at index <pos>. 
//Every index is adjusted accordingly.
//Returns negative on errors.
int  dhex_bxg_delete_frame(dhex_bxg* bxg, int pos); 

void      dhex_save_bxg(dhex_bxg* bxg, const char* filename);
dhex_bxg* dhex_load_bxg(const char* filename);

#endif
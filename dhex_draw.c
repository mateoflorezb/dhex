#include "dhex_draw.h"
#include "stb_image.h"
#include "stb_image_write.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

static inline float fround(float v) {
	float f = floor(v);
	if (v - f < 0.5) return f;
	return ceil(v);
}

static inline int iclamp(int val, int min, int max) {
	if (val < min) return min;
	if (val > max) return max;
	return val;
}

int dhex_draw_err = 0;
char dhex_draw_err_msg[_DHEX_ERROR_CHARS] = "[dhex] no error";

void dhex_clear_error(void) {
	dhex_draw_err = 0; 
	sprintf(dhex_draw_err_msg, "[dhex] no error\0");	
}

//pixel functions are not draw calls. These won't check if x,y coords make sense for the canvas
//////////////////////////////////////////////////////////////////////
static inline void dhex_pixel_set  (dhex_rgba color, dhex_canvas* cv, int x, int y) {
	//dhex_rgba prev = cv->pixelbuffer.pixeldata[x + cv->pixelbuffer.w*y];
	if (color.a > 127) {
		color.a = 255;
		cv->pixelbuffer.pixeldata[x + cv->pixelbuffer.w*y] = color;
	}
}

//////////////////////////////////////////////////////////////////////
static inline void dhex_pixel_blend(dhex_rgba color, dhex_canvas* cv, int x, int y) {
	float a = ((float)color.a) / 255.0;
	float ra = 1.0 - a;
	
	dhex_rgba prev = cv->pixelbuffer.pixeldata[x + cv->pixelbuffer.w*y];
	cv->pixelbuffer.pixeldata[x + cv->pixelbuffer.w*y] = (dhex_rgba) {
		.r = iclamp ( (int)floor( ( (float)prev.r) * ra) + (int)floor( ( (float)color.r) *  a) , 0,255),
		.g = iclamp ( (int)floor( ( (float)prev.g) * ra) + (int)floor( ( (float)color.g) *  a) , 0,255),
		.b = iclamp ( (int)floor( ( (float)prev.b) * ra) + (int)floor( ( (float)color.b) *  a) , 0,255),
		.a = iclamp(prev.a + color.a, 0, 255)
	};
}

//////////////////////////////////////////////////////////////////////
static inline void dhex_pixel_sub  (dhex_rgba color, dhex_canvas* cv, int x, int y) {
	
	u8 prev_a = cv->pixelbuffer.pixeldata[x + cv->pixelbuffer.w*y].a;
	u8 new_a = iclamp((int)prev_a - (int)color.a, 0, 255);
	cv->pixelbuffer.pixeldata[x + cv->pixelbuffer.w*y].a = new_a;
}

//////////////////////////////////////////////////////////////////////
void dhex_save_palette(dhex_palette* pal, const char* filename) {
	
	if (strlen(filename) > _DHEX_MAX_FILENAME_LENGTH) {
		
		dhex_draw_err = DHEX_ERROR_PALETTE_OPEN;
		sprintf(dhex_draw_err_msg, "[dhex] Error opening %s: filename too long\0", filename);	
		return;
	}
	
	FILE* f = fopen(filename, "wb");
	char issue = 0;
	u8 length = pal->ncolors;
	fwrite(&length, 1, 1, f);
	
	if (ferror(f) != 0) {
		dhex_draw_err = DHEX_ERROR_PALETTE_WRITE;
		sprintf(dhex_draw_err_msg, "[dhex] Error saving palette %s\0", filename);
		return;
	}
	
	if (f) {
	
		for (int k = 1; k <= (int)length; k++) {

			fwrite(&(pal->colors[k]), sizeof(dhex_rgba), 1, f);
														
			if (ferror(f) != 0) {
				dhex_draw_err = DHEX_ERROR_PALETTE_WRITE;
				sprintf(dhex_draw_err_msg, "[dhex] Error saving palette %s\0", filename);
				return;
			}
			
		}
		
	}
	else {
		
		dhex_draw_err = DHEX_ERROR_PALETTE_OPEN;
		sprintf(dhex_draw_err_msg, "[dhex] Could not open %s\0", filename);	
		return;		
	}
	
	
	fclose(f);
}


//////////////////////////////////////////////////////////////////////
dhex_palette* dhex_load_palette(const char* filename) {
	
	if (strlen(filename) > _DHEX_MAX_FILENAME_LENGTH) {
		
		dhex_draw_err = DHEX_ERROR_PALETTE_OPEN;
		sprintf(dhex_draw_err_msg, "[dhex] Error opening %s: filename too long\0", filename);	
		return NULL;
	}
	
	dhex_palette* pal = NULL;
	FILE* f = fopen(filename, "rb");
	u8 length = 0;
	
	if (f) {
		
		pal = malloc(sizeof(dhex_palette));
		memset(pal, 0, sizeof(dhex_palette));
		fread(&length, sizeof(u8), 1, f);
		char issue = 0;
		for (int k = 1; k <= (int)length; k++) {
			if( feof(f) == 0) { //not at the end of the file
			
				fread(&(pal->colors[k]), sizeof(dhex_rgba), 1, f); //done color by color cuz idk if 
															//the compiler wants to add padding (miss me with that shit)
				if (ferror(f) != 0) {
					issue = 1; //stream error
				}
				
			}
			else {
				issue = 1;	//unexpected eof, palette file is larger than it should.
			}
			
			if (issue == 1) {
				
				dhex_draw_err = DHEX_ERROR_PALETTE_READ;
				sprintf(dhex_draw_err_msg, "[dhex] Error reading palette %s\0", filename);
				free(pal);
				return NULL;
			}	
		}
	}
	else {
		
		dhex_draw_err = DHEX_ERROR_PALETTE_OPEN;
		sprintf(dhex_draw_err_msg, "[dhex] Could not open %s\0", filename);	
		return NULL;		
	}
	
	fclose(f);
	pal->ncolors = length;
	return pal;
}

//////////////////////////////////////////////////////////////////////
dhex_image* dhex_load_image_png(const char* filename) {
	
	if (strlen(filename) > _DHEX_MAX_FILENAME_LENGTH) {
		
		dhex_draw_err = DHEX_ERROR_IMAGE_OPEN;
		sprintf(dhex_draw_err_msg, "[dhex] Error opening %s: filename too long\0", filename);	
		return NULL;
	}
	
	int w, h, n;
	u8* data = stbi_load(filename, &w, &h, &n, 4);
	if (data != NULL) {
		dhex_image* new_img = malloc(sizeof(dhex_image));
		new_img->w = w;
		new_img->h = h;
		new_img->pixeldata = (dhex_rgba*)data;
		return new_img;
	}
	else {
		dhex_draw_err = DHEX_ERROR_IMAGE_OPEN;
		sprintf(dhex_draw_err_msg, "[dhex] Could not open %s\0", filename);			
		return NULL;
	
	}
}

//////////////////////////////////////////////////////////////////////
int dhex_dump_image_png(const char* filename, dhex_image* mg) {
	stbi_write_png(filename, mg->w, mg->h, 4, mg->pixeldata, 4 * mg->w);
}



//////////////////////////////////////////////////////////////////////
void dhex_free_image(dhex_image* mg) {
	stbi_image_free(mg->pixeldata);
	free(mg);
}


//////////////////////////////////////////////////////////////////////
dhex_sprite* dhex_load_sprite_png(const char* filename) {
	if (strlen(filename) > _DHEX_MAX_FILENAME_LENGTH) {
		
		dhex_draw_err = DHEX_ERROR_SPRITE_OPEN;
		sprintf(dhex_draw_err_msg, "[dhex] Error opening %s: filename too long\0", filename);	
		return NULL;
	}
	
	int w, h, n;
	u8* data = stbi_load(filename, &w, &h, &n, 4);
	if (data != NULL) {
		dhex_sprite* new_spr = malloc(sizeof(dhex_sprite));
		new_spr->w = w;
		new_spr->h = h;
		new_spr->pixeldata = malloc(sizeof(u8)*w*h);
		
		for (int yy = 0; yy < h; yy++)
		for (int xx = 0; xx < w; xx++) {
			new_spr->pixeldata[xx + w*yy] = data[4*(xx + w*yy)];
		}
		
		stbi_image_free(data);
		return new_spr;
	}
	else {
		dhex_draw_err = DHEX_ERROR_SPRITE_OPEN;
		sprintf(dhex_draw_err_msg, "[dhex] Could not open %s\0", filename);			
		return NULL;
	
	}	
	
}

//////////////////////////////////////////////////////////////////////
int dhex_dump_sprite_bin(dhex_sprite* spr, const char* filename) {
	/*
	Binary sprite format:

	int32_t w
	int32_t h
	and then the palette indexes as unsigned chars, each taking 1 byte 

	*/

	FILE* f = fopen(filename, "wb");
	if (f) {
		int32_t w = spr->w;
		int32_t h = spr->h;
		
		fwrite(&w, 4, 1, f); 
		fwrite(&h, 4, 1, f); 
		
		for (int yy = 0; yy < h; yy++)
		for (int xx = 0; xx < w; xx++) {
			fwrite(&(spr->pixeldata[xx + w*yy]), 1, 1, f); 
		}
		
		fclose(f);
		return 0;
	}
	else return -1;
	
}

//////////////////////////////////////////////////////////////////////
dhex_sprite* dhex_load_sprite_bin(const char* filename) {
	
	FILE* f = fopen(filename, "rb");
	if (f) {
		
		dhex_sprite* spr = malloc(sizeof(dhex_sprite));

		int32_t w;
		int32_t h;
		
		fread(&w, 4, 1, f); 
		fread(&h, 4, 1, f); 
		
		spr->w = w;
		spr->h = h;
		
		spr->pixeldata = malloc(sizeof(u8)*w*h);
		
		for (int yy = 0; yy < h; yy++)
		for (int xx = 0; xx < w; xx++) {
			fread(&(spr->pixeldata[xx + w*yy]), 1, 1, f); 
		}
		
		fclose(f);
		return spr;
	}
	else return NULL;	
	
}

//////////////////////////////////////////////////////////////////////
void dhex_free_sprite(dhex_sprite* spr) {
	free(spr->pixeldata);
	free(spr);
}


//////////////////////////////////////////////////////////////////////
dhex_canvas* dhex_create_canvas(int32_t w, int32_t h) {
	
	dhex_canvas* cv = malloc(sizeof(dhex_canvas));
	cv->pixelbuffer.w = w;
	cv->pixelbuffer.h = h;
	cv->pixelbuffer.pixeldata = malloc(sizeof(dhex_rgba)*w*h);
	
	cv->current_layer = DHEX_LAYER_0;
	cv->colormode = DHEX_COLORMODE_SET;
	
	for (int k = 0; k < DHEX_LAYER_MAX; k++) {
		cv->layer[k] = clist_create(_dhex_draw_call_batch*);
	}
	
	return cv;
	
}


//////////////////////////////////////////////////////////////////////
void dhex_free_canvas(dhex_canvas* cv) {
	free(cv->pixelbuffer.pixeldata);
	
	for (int k = 0; k < DHEX_LAYER_MAX; k++) {
		
		clist_iterator it = clist_start(cv->layer[k]);
		while (it != NULL) {
			_dhex_draw_call_batch* batch = clist_get(it, _dhex_draw_call_batch*);
			free(batch);
			it = clist_next(it);
		}
		clist_free(cv->layer[k]);
	}	
	
}

//////////////////////////////////////////////////////////////////////
void dhex_set_layer(u8 layer, dhex_canvas* cv) {
	cv->current_layer = layer;	
}

void dhex_set_colormode(u8 colormode, dhex_canvas* cv) {
	cv->colormode = colormode;
}

static inline void _local_dhex_push_draw_call(dhex_canvas* cv, _dhex_draw_call dc) {
	clist_iterator it = clist_start(cv->layer[cv->current_layer]);
	_dhex_draw_call_batch* batch = NULL;
	char found = 0;
	while (it != NULL) {
		batch = clist_get(it, _dhex_draw_call_batch*);
		if (batch->current < DHEX_DRAW_CALL_BATCH_SIZE) {
			found = 1;
			break;
		}
		it = clist_next(it);
	}
	
	if (found == 0) {
		batch = malloc(sizeof(_dhex_draw_call_batch));
		batch->current = 0;
		clist_push(cv->layer[cv->current_layer], &batch);
	}
	
	batch->draw_calls[batch->current] = dc;
	batch->current++;
	
}

//////////////////////////////////////////////////////////////////////
void dhex_draw_rect(dhex_rgba color, dhex_canvas* cv, int x, int y, int w, int h) {
	
	_dhex_draw_call dc;
	dc.type = _DHEX_DRAW_CALL_RECT;
	dc.colormode = cv->colormode;
	
	dc.data.rect.color = color;
	dc.data.rect.x = x;
	dc.data.rect.y = y;
	dc.data.rect.w = w;
	dc.data.rect.h = h;
	
	_local_dhex_push_draw_call(cv, dc);
	
}

//////////////////////////////////////////////////////////////////////
void dhex_clear_canvas(dhex_canvas* cv) {
	
	_dhex_draw_call dc;
	dc.type = _DHEX_DRAW_CALL_CLEAR;
	
	_local_dhex_push_draw_call(cv, dc);
	
}

//////////////////////////////////////////////////////////////////////
void dhex_draw_image(dhex_image* mg, dhex_canvas* cv, int x, int y, int xOffset, int yOffset) {
	
	_dhex_draw_call dc;
	dc.type = _DHEX_DRAW_CALL_IMAGE;
	dc.colormode = cv->colormode;
	
	dc.data.image.mg = mg;
	dc.data.image.x = x;
	dc.data.image.y = y;
	dc.data.image.xOffset = xOffset;
	dc.data.image.yOffset = yOffset;
	
	_local_dhex_push_draw_call(cv, dc);
	
}

//////////////////////////////////////////////////////////////////////
void dhex_draw_image_part(dhex_image* mg, dhex_canvas* cv, int x, int y, int xOffset, int yOffset,
	int xSection, int ySection, int wSection, int hSection) {

	_dhex_draw_call dc;
	dc.type = _DHEX_DRAW_CALL_IMAGE_PART;
	dc.colormode = cv->colormode;
	
	dc.data.image_part.mg = mg;
	dc.data.image_part.x = x;
	dc.data.image_part.y = y;
	dc.data.image_part.xOffset = xOffset;
	dc.data.image_part.yOffset = yOffset;
	dc.data.image_part.xSection = xSection;
	dc.data.image_part.ySection = ySection;
	dc.data.image_part.wSection = wSection;
	dc.data.image_part.hSection = hSection;
	
	_local_dhex_push_draw_call(cv, dc);
		
}


//////////////////////////////////////////////////////////////////////
void dhex_draw_sprite(dhex_sprite* spr, dhex_palette* pal,
dhex_canvas* cv, int x, int y, int xOffset, int yOffset) {
	
	_dhex_draw_call dc;
	dc.type = _DHEX_DRAW_CALL_SPRITE;
	dc.colormode = cv->colormode;
	
	dc.data.sprite.spr = spr;
	dc.data.sprite.pal = pal;
	dc.data.sprite.x = x;
	dc.data.sprite.y = y;
	dc.data.sprite.xOffset = xOffset;
	dc.data.sprite.yOffset = yOffset;
	
	_local_dhex_push_draw_call(cv, dc);
	
}

//////////////////////////////////////////////////////////////////////
void dhex_draw_sprite_part(dhex_sprite* spr, dhex_palette* pal, dhex_canvas* cv, int x, int y, int xOffset, int yOffset,
	int xSection, int ySection, int wSection, int hSection) {
	
	_dhex_draw_call dc;
	dc.type = _DHEX_DRAW_CALL_SPRITE_PART;
	dc.colormode = cv->colormode;
	
	dc.data.sprite_part.spr = spr;
	dc.data.sprite_part.pal = pal;
	dc.data.sprite_part.x = x;
	dc.data.sprite_part.y = y;
	dc.data.sprite_part.xOffset = xOffset;
	dc.data.sprite_part.yOffset = yOffset;
	dc.data.sprite_part.xSection = xSection;
	dc.data.sprite_part.ySection = ySection;
	dc.data.sprite_part.wSection = wSection;
	dc.data.sprite_part.hSection = hSection;
	
	_local_dhex_push_draw_call(cv, dc);
	
}

//////////////////////////////////////////////////////////////////////
void dhex_draw_sprite_ex(dhex_sprite* spr, dhex_palette* pal, dhex_canvas* cv,
int x, int y, int xOffset, int yOffset, float xscale, float yscale) {
	
	_dhex_draw_call dc;
	dc.type = _DHEX_DRAW_CALL_SPRITE_EX;
	dc.colormode = cv->colormode;
	
	dc.data.sprite_ex.spr = spr;
	dc.data.sprite_ex.pal = pal;
	dc.data.sprite_ex.x = x;
	dc.data.sprite_ex.y = y;
	dc.data.sprite_ex.xOffset = xOffset;
	dc.data.sprite_ex.yOffset = yOffset;
	dc.data.sprite_ex.xscale = xscale;
	dc.data.sprite_ex.yscale = yscale;
	
	_local_dhex_push_draw_call(cv, dc);
	
}

//////////////////////////////////////////////////////////////////////
void dhex_draw_image_ex(dhex_image* mg, dhex_canvas* cv, int x, int y,
		int xOffset, int yOffset, float xscale, float yscale) {
			
	_dhex_draw_call dc;
	dc.type = _DHEX_DRAW_CALL_IMAGE_EX;
	dc.colormode = cv->colormode;
	
	dc.data.image_ex.mg = mg;
	dc.data.image_ex.x = x;
	dc.data.image_ex.y = y;
	dc.data.image_ex.xOffset = xOffset;
	dc.data.image_ex.yOffset = yOffset;
	dc.data.image_ex.xscale = xscale;
	dc.data.image_ex.yscale = yscale;
	
	_local_dhex_push_draw_call(cv, dc);
			
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

//if pal == NULL, function will assume we are rendering an img. otherwise, it will assume it's a spr 
static inline void _local_dhex_draw_picture(void (*pixel_function)(dhex_rgba, dhex_canvas*, int, int), 
void* img_or_spr, dhex_palette* pal, dhex_canvas* cv, int x, int y, int xOffset, int yOffset) {
	
	register int xx;
	register int yy;
	
	int w, h;

	xx = x - xOffset;
	yy = y - yOffset;
	
	if (pal == NULL) {

		w = ((dhex_image*)img_or_spr)->w;
		h = ((dhex_image*)img_or_spr)->h;
	}
	else {
		
		w = ((dhex_sprite*)img_or_spr)->w;
		h = ((dhex_sprite*)img_or_spr)->h;		
	}

	if (xx >= cv->pixelbuffer.w
	||  yy >= cv->pixelbuffer.h) return;

	if (xx < 0) { 
		w += xx; 
		xx = 0;
	}

	if (yy < 0) { 
		h += yy; 
		yy = 0;
	}

	w = iclamp(w, 0, cv->pixelbuffer.w - xx);
	h = iclamp(h, 0, cv->pixelbuffer.h - yy);

	//if this is true after all these adjustments, then we have
	//a rectangle inside the canvas with actual area
	if (w > 0 && h > 0) {
		
		int img_xStart = xx - (x - xOffset);
		int img_yStart = yy - (y - yOffset);

		
		register int pxLim = xx + w;
		register int pyLim = yy + h;
		
		if (pal == NULL) {
		
			for (register int py = yy; py < pyLim; py++)
			for (register int px = xx; px < pxLim; px++) {
				
				register int mgx = img_xStart + (px - xx);
				register int mgy = img_yStart + (py - yy);
				
				pixel_function(  ((dhex_image*)img_or_spr)->pixeldata[mgx + ((dhex_image*)img_or_spr)->w * mgy],
				cv, px, py);
			}
			
		}
		else {
			
			for (register int py = yy; py < pyLim; py++)
			for (register int px = xx; px < pxLim; px++) {
				
				register int mgx = img_xStart + (px - xx);
				register int mgy = img_yStart + (py - yy);
				
				dhex_rgba color = pal->colors[
					((dhex_sprite*)img_or_spr)->pixeldata[mgx + ((dhex_sprite*)img_or_spr)->w * mgy]
				];
				pixel_function(color, cv, px, py);
			}
		}
	}		
	
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//if pal == NULL, function will assume we are rendering an img. otherwise, it will assume it's a spr 
static inline void _local_dhex_draw_picture_part(void (*pixel_function)(dhex_rgba, dhex_canvas*, int, int), 
void* img_or_spr, dhex_palette* pal, dhex_canvas* cv, int x, int y, int xOffset, int yOffset,
int xSection, int ySection, int wSection, int hSection) {

	register int sw;
	int sh;
	if (pal == NULL) {
		sw = ((dhex_image*)img_or_spr)->w;
		sh = ((dhex_image*)img_or_spr)->h;
	}
	else {
		sw = ((dhex_sprite*)img_or_spr)->w;
		sh = ((dhex_sprite*)img_or_spr)->h;		
	}
	
	if (xSection < 0) {
		wSection += xSection;
		xSection = 0;
		
	}
	
	if (ySection < 0) {
		hSection += ySection;
		ySection = 0;
	}	
	
	wSection = iclamp(wSection, 0, sw - xSection);
	hSection = iclamp(hSection, 0, sh - ySection);
	
	//now, x/y/w/h Section properly describe what area of the source picture we are focusing on 
	
	if (wSection > 0 && hSection > 0) {
		int screen_xOrigin = x - xOffset + xSection;
		int screen_yOrigin = y - yOffset + ySection;
		
		register int screen_xStart = screen_xOrigin; //xOrigin is where the actual origin of the rect is. xStart is where
		register int screen_yStart = screen_yOrigin; //it starts when clamped to screen range
		
		if (screen_xOrigin < 0) {
			wSection += screen_xOrigin;
			screen_xStart = 0;
			//xSection += screen_xOrigin; /////////////// EXPERIMENTAL
		}
		
		if (screen_yOrigin < 0) {
			hSection += screen_yOrigin;
			screen_yStart = 0;
			//ySection += screen_yOrigin; /////////////// EXPERIMENTAL
		}
		
		wSection = iclamp(wSection, 0, cv->pixelbuffer.w - screen_xStart);
		hSection = iclamp(hSection, 0, cv->pixelbuffer.h - screen_yStart);
		
		//now, screen_x/xStart and w/hSection describe the rect on screen that the image should output to 
		
		register int screen_xLim = screen_xStart + wSection;
		register int screen_yLim = screen_yStart + hSection;
		
		register int img_xStart = xSection + (screen_xStart - screen_xOrigin);
		register int img_yStart = ySection + (screen_yStart - screen_yOrigin);
		
		if (pal == NULL) {
		
			for (register int py = screen_yStart; py < screen_yLim; py++)
			for (register int px = screen_xStart; px < screen_xLim; px++) {
				
				register int mgx = img_xStart + (px - screen_xStart);
				register int mgy = img_yStart + (py - screen_yStart);
				
				pixel_function(  ((dhex_image*)img_or_spr)->pixeldata[mgx + sw * mgy],
				cv, px, py);
			}
			
		}
		else {
			
			for (register int py = screen_yStart; py < screen_yLim; py++)
			for (register int px = screen_xStart; px < screen_xLim; px++) {
				
				register int mgx = img_xStart + (px - screen_xStart);
				register int mgy = img_yStart + (py - screen_yStart);
				
				dhex_rgba color = pal->colors[
					((dhex_sprite*)img_or_spr)->pixeldata[mgx + sw * mgy]
				];
				pixel_function(color, cv, px, py);
			}			
			
		}
		
	}
	
}


//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//if pal == NULL, function will assume we are rendering an img. otherwise, it will assume it's a spr 
static inline void _local_dhex_draw_picture_ex(void (*pixel_function)(dhex_rgba, dhex_canvas*, int, int), 
void* img_or_spr, dhex_palette* pal, dhex_canvas* cv, int x, int y, int xOffset, int yOffset, float xscale, float yscale) {
		
	int xCorner, yCorner; //image top left origin on canvas (accounting for scaling and offset)
	
	int sw, sh;
	if (pal == NULL) {
		sw = ((dhex_image*)img_or_spr)->w;
		sh = ((dhex_image*)img_or_spr)->h;
	}
	else {
		sw = ((dhex_sprite*)img_or_spr)->w;
		sh = ((dhex_sprite*)img_or_spr)->h;		
	}

	if (xscale > 0) 
		xCorner = x - floor( ((float)xOffset) * xscale);
	else 
		xCorner = x + floor( (float)(sw - xOffset) * xscale);
	
	if (yscale > 0) 
		yCorner = y - floor( ((float)yOffset) * yscale);
	else 
		yCorner = y + floor( (float)(sh - yOffset) * yscale);
	
	
	int w = floor( ((float)sw) * xscale);

	if (w < 0) w = -w; 

	int h = floor( ((float)sh) * yscale);

	if (h < 0) h = -h;

	register int full_w = w;
	register int full_h = h;

	//xCorner, yCorner, w and h describe the area that the scaled image occupies

	int xx = xCorner;
	int yy = yCorner;

	if (xx < 0) {
		w += xx;
		xx = 0;
	}

	if (yy < 0) {
		h += yy;
		yy = 0;
	}

	w = iclamp(w, 0, cv->pixelbuffer.w - xx);
	h = iclamp(h, 0, cv->pixelbuffer.h - yy);

	//now xx,yy,w and h describe the clamped area the scaled image occupies 

	register int xlim = xx + w;
	register int ylim = yy + h;
	
	register int mgx, mgy; //aka what pixel from the img are we targeting

	if (pal == NULL) {
		
		for (register int py = yy; py < ylim; py++)
		for (register int px = xx; px < xlim; px++) {
			
			mgx = fround( ((float)sw) * ((float)(px - xCorner) / (float)(full_w)) );
				
			if (xscale < 0) {
				mgx = (sw - 1) - mgx;
			}
			
			mgy = fround( ((float)sh) * ((float)(py - yCorner) / (float)(full_h)) );	
			
			if (yscale < 0) {
				mgy = (sh - 1) - mgy;
			}
			
			//since round() can make (mgx, mgy) coords to be outside the picture's dimensions
			//we gotta clamp like this 
			if (mgx >= sw) mgx = sw - 1;
			if (mgy >= sh) mgy = sh - 1;
			
			pixel_function(  ((dhex_image*)img_or_spr)->pixeldata[mgx + sw * mgy], cv, px, py);		
		}
	
	}
	else {
		
		for (register int py = yy; py < ylim; py++)
		for (register int px = xx; px < xlim; px++) {
			
			mgx = fround( ((float)sw) * ((float)(px - xCorner) / (float)(full_w)) );
				
			if (xscale < 0) {
				mgx = (sw - 1) - mgx;
			}
			
			mgy = fround( ((float)sh) * ((float)(py - yCorner) / (float)(full_h)) );	
			
			if (yscale < 0) {
				mgy = (sh - 1) - mgy;
			}
			
			//since round() can make (mgx, mgy) coords to be outside the picture's dimensions
			//we gotta clamp like this 
			if (mgx >= sw) mgx = sw - 1;
			if (mgy >= sh) mgy = sh - 1;
			
			dhex_rgba color = pal->colors[
				((dhex_sprite*)img_or_spr)->pixeldata[mgx + sw * mgy]
			];
			pixel_function(color, cv, px, py);		
		}			
		
	}
	
	
}

void dhex_render_canvas(dhex_canvas* cv) {
	
	for (int k = 0; k < DHEX_LAYER_MAX; k++) {
		
		clist_iterator it = clist_start(cv->layer[k]);
		
		while (it != NULL) {
			
			clist_iterator next = clist_next(it);
			
			_dhex_draw_call_batch* batch = clist_get(it, _dhex_draw_call_batch*);
			
			int batchN = batch->current;
			for (int dci = 0; dci < batch->current; dci++) {
				
				_dhex_draw_call dc = batch->draw_calls[dci];
				
				void (*pixel_function)(dhex_rgba, dhex_canvas*, int, int) = NULL;
				switch(dc.colormode) {
					
					case DHEX_COLORMODE_SET:       pixel_function = &dhex_pixel_set;   break;
					case DHEX_COLORMODE_BLEND:     pixel_function = &dhex_pixel_blend; break;
					case DHEX_COLORMODE_SUB_ALPHA: pixel_function = &dhex_pixel_sub;   break;
					
					
					default: break;
				}
			
				switch(dc.type) {
					
					case _DHEX_DRAW_CALL_CLEAR:
					{				
						memset(cv->pixelbuffer.pixeldata, 0,
							sizeof(dhex_rgba) * cv->pixelbuffer.w * cv->pixelbuffer.h);
					}
					break;
					
					
					case _DHEX_DRAW_CALL_RECT:
					{
						if (dc.data.rect.x >= cv->pixelbuffer.w
						||  dc.data.rect.y >= cv->pixelbuffer.h) break;
						
						if (dc.data.rect.x < 0) { 
							dc.data.rect.w += dc.data.rect.x; 
							dc.data.rect.x = 0;
						}
						
						if (dc.data.rect.y < 0) { 
							dc.data.rect.h += dc.data.rect.y; 
							dc.data.rect.y = 0;
						}
						
						dc.data.rect.w = iclamp(dc.data.rect.w, 0, cv->pixelbuffer.w - dc.data.rect.x);
						dc.data.rect.h = iclamp(dc.data.rect.h, 0, cv->pixelbuffer.h - dc.data.rect.y);
						
						//if this is true after all these adjustments, then we have
						//a rectangle inside the canvas with actual area
						if (dc.data.rect.w > 0 && dc.data.rect.h > 0) {
							int ylim = dc.data.rect.y + dc.data.rect.h;
							int xlim = dc.data.rect.x + dc.data.rect.w;
							
							for (int py = dc.data.rect.y; py < ylim; py++)
							for (int px = dc.data.rect.x; px < xlim; px++) {
								
								pixel_function(dc.data.rect.color, cv, px, py);
							}	
						}
					}
					break;
					
					
					case _DHEX_DRAW_CALL_IMAGE:
					{
						_local_dhex_draw_picture(pixel_function, dc.data.image.mg , NULL, cv,
							dc.data.image.x, dc.data.image.y, dc.data.image.xOffset, dc.data.image.yOffset);
					}
					break;
					
					
					
					case _DHEX_DRAW_CALL_IMAGE_EX:
					{
						_local_dhex_draw_picture_ex(pixel_function, (void*)dc.data.image_ex.mg, NULL, cv, dc.data.image_ex.x,
						dc.data.image_ex.y, dc.data.image_ex.xOffset, dc.data.image_ex.yOffset, dc.data.image_ex.xscale, dc.data.image_ex.yscale);	
					}
					break;
					
					case _DHEX_DRAW_CALL_IMAGE_PART:
					{
						_local_dhex_draw_picture_part(pixel_function, (void*)dc.data.image_part.mg, NULL, cv, dc.data.image_part.x,
						dc.data.image_part.y, dc.data.image_part.xOffset, dc.data.image_part.yOffset,
						dc.data.image_part.xSection, dc.data.image_part.ySection, dc.data.image_part.wSection, dc.data.image_part.hSection);
					}
					break;
					
					/////////////////////////////////////////////////////////////////
					// Now sprites //////////////////////////////////////////////////
					/////////////////////////////////////////////////////////////////
					
					case _DHEX_DRAW_CALL_SPRITE:
					{
						_local_dhex_draw_picture(pixel_function, dc.data.sprite.spr , dc.data.sprite.pal, cv,
							dc.data.sprite.x, dc.data.sprite.y, dc.data.sprite.xOffset, dc.data.sprite.yOffset);
					}
					break;
					
					case _DHEX_DRAW_CALL_SPRITE_PART:
					{
						_local_dhex_draw_picture_part(pixel_function, (void*)dc.data.sprite_part.spr, dc.data.sprite_part.pal, cv,
						dc.data.sprite_part.x, dc.data.sprite_part.y, dc.data.sprite_part.xOffset, dc.data.sprite_part.yOffset,
						dc.data.sprite_part.xSection, dc.data.sprite_part.ySection, dc.data.sprite_part.wSection, dc.data.sprite_part.hSection);
					}
					break;
					
					case _DHEX_DRAW_CALL_SPRITE_EX:
					{
						_local_dhex_draw_picture_ex(pixel_function, (void*)dc.data.sprite_ex.spr, dc.data.sprite_ex.pal, cv, dc.data.sprite_ex.x,
						dc.data.sprite_ex.y, dc.data.sprite_ex.xOffset, dc.data.sprite_ex.yOffset, dc.data.sprite_ex.xscale, dc.data.sprite_ex.yscale);	
					}
					break;				
			
					
					default: break;
				}
			}
			
			free(batch);
			clist_delete(it);
			it = next;
		}
	}	
}
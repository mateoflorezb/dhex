#ifndef __dhex_draw_h
#define __dhex_draw_h

#define DHEX_LAYER_MAX 16

#define DHEX_COLORMODE_SET 0
#define DHEX_COLORMODE_BLEND 1
#define DHEX_COLORMODE_SUB_ALPHA 2

#define _DHEX_ERROR_CHARS 400
#define _DHEX_MAX_FILENAME_LENGTH 200

#define DHEX_FIXED_SCALE 256

#define DHEX_DRAW_CALL_BATCH_SIZE 128

#include <stdint.h>
#include "clist.h"


typedef unsigned char u8;

extern int dhex_draw_err;
extern char dhex_draw_err_msg[_DHEX_ERROR_CHARS];

enum _DHEX_ERROR_CODES {
	DHEX_ERROR_NONE = 0,
	DHEX_ERROR_PALETTE_OPEN = 1, //failed to open the file 
	DHEX_ERROR_PALETTE_READ, //failed to reach eof at the expected moment
	DHEX_ERROR_PALETTE_WRITE, //stream error while writing a palette file 
	DHEX_ERROR_IMAGE_OPEN,
	DHEX_ERROR_SPRITE_OPEN
	
};

void dhex_clear_error(void);

enum __DHEX_LAYERS {
	DHEX_LAYER_0 = 0,
	DHEX_LAYER_1,
	DHEX_LAYER_2,
	DHEX_LAYER_3,
	DHEX_LAYER_4,
	DHEX_LAYER_5,
	DHEX_LAYER_6,
	DHEX_LAYER_7,
	DHEX_LAYER_8,
	DHEX_LAYER_9,
	DHEX_LAYER_10,
	DHEX_LAYER_11,
	DHEX_LAYER_12,
	DHEX_LAYER_13,
	DHEX_LAYER_14,
	DHEX_LAYER_15
};

typedef struct {
	u8 r, g, b, a;
} dhex_rgba;

/*
palette v2 format:
first byte: uint8_t that defines the largest defined index
(0 is always defined, since it must be alpha = 0)
then the indexes are defined in order (starting from index 1,
as index 0 is always predefined), each one taking 4 bytes 
*/

typedef struct {
	u8 ncolors; //aka max defined index. Mostly for saving/loading palette files 
	dhex_rgba colors[256]; //color index 0 will always be no color (alpha = 0), but let's store it anyway
							//for simplicity/efficiency reasons later on.
} dhex_palette;

dhex_palette* dhex_load_palette(const char* filename); //palgen v2 file
void          dhex_save_palette(dhex_palette* pal, const char* filename); //palgen v2 file
//free palettes with free()

typedef struct {
	int w,h;
	dhex_rgba* pixeldata; //access with x + wy
} dhex_image;

//palettized image. Each pixel is a char that points to certain palette index.
//drawing functions for sprites should provide a palette argument.
//sprites are loaded from png images. It will use the R-channel (like palgen files)
typedef struct {
	int w,h;
	u8* pixeldata; //access with x + wy
} dhex_sprite; 

dhex_image* dhex_load_image_png(const char* filename);
dhex_image* dhex_load_image_bin(const char* filename); 
//int dhex_dump_image_bin(const char* filename, dhex_image* mg); //return negative on error <<<<<<< TO DO
int dhex_dump_image_png(const char* filename, dhex_image* mg); //return negative on error. Useful for taking screenshots of the game maybe?
void dhex_free_image(dhex_image* mg);


/*
Binary sprite format:

int32_t w
int32_t h
and then the palette indexes as unsigned chars, each taking 1 byte 

*/

dhex_sprite* dhex_load_sprite_png(const char* filename);
dhex_sprite* dhex_load_sprite_bin(const char* filename);
int dhex_dump_sprite_bin(dhex_sprite* spr, const char* filename); // return negative on error
void dhex_free_sprite(dhex_sprite* spr);

//the idea with also supporting bin image and sprite loading is to
//increase portability. Idk if stb image is supported on every platform,
//but stdio.h most likely is.

typedef struct {
	dhex_image pixelbuffer;
	clist* layer[DHEX_LAYER_MAX];
	u8 current_layer;
	u8 colormode; //see DHEX_COLORMODE_... macros
	
} dhex_canvas;

dhex_canvas* dhex_create_canvas(int w, int h);
void dhex_free_canvas(dhex_canvas* cv);
void dhex_render_canvas(dhex_canvas* cv); //attends (and flushes) every queued draw call.
//The resulting drawing lies in the canvas' pixelbuffer

enum _DHEX_DRAW_CALL_TYPES {
	_DHEX_DRAW_CALL_IMAGE = 0,
	_DHEX_DRAW_CALL_IMAGE_EX,
	_DHEX_DRAW_CALL_IMAGE_PART,
	_DHEX_DRAW_CALL_SPRITE,
	_DHEX_DRAW_CALL_SPRITE_EX,
	_DHEX_DRAW_CALL_SPRITE_PART,
	_DHEX_DRAW_CALL_RECT,
	_DHEX_DRAW_CALL_CLEAR
};

typedef struct {
	uint8_t type;
	u8 colormode;
	union {
		
		struct {
			dhex_image* mg;
			int x;
			int y;
			int xOffset;
			int yOffset;
		} image;
		
		struct {
			dhex_image* mg;
			int x;
			int y;
			int xOffset;
			int yOffset;
			float xscale;
			float yscale;
		} image_ex;
		
		struct {
			dhex_image* mg;
			int x;
			int y;
			int xOffset;
			int yOffset;
			int xSection;
			int ySection;
			int wSection;
			int hSection;
			
		} image_part;
			
		struct {
			dhex_sprite* spr;
			dhex_palette* pal;
			int x;
			int y;
			int xOffset;
			int yOffset;
		} sprite;
		
		struct {
			dhex_sprite* spr;
			dhex_palette* pal;
			int x;
			int y;
			int xOffset;
			int yOffset;
			float xscale;
			float yscale;
		} sprite_ex;	

		struct {
			dhex_sprite* spr;
			dhex_palette* pal;
			int x;
			int y;
			int xOffset;
			int yOffset;
			int xSection;
			int ySection;
			int wSection;
			int hSection;
			
		} sprite_part;		
			
		struct {
			dhex_rgba color;
			int x;
			int y;
			int w;
			int h;
		} rect;			
		
	} data;
	
} _dhex_draw_call;


//DHEX stores draw calls in batches of (by default) 128 draw calls. When a new draw call is issued,
//it will be stored in the last created batch (if it isn't full yet). If this batch happens to be full,
//a new empty batch is created and the draw call is added to this new batch. This was done as to not 
//call malloc once for each draw call, but instead once each 128 draw calls (since heap allocation is kinda slow,
//I suspected this would fuck up performance. Turns out, it did, performance did improve after this batch system)

typedef struct {
	
	int current;
	_dhex_draw_call draw_calls[DHEX_DRAW_CALL_BATCH_SIZE];
	
} _dhex_draw_call_batch;

void dhex_set_layer(u8 layer, dhex_canvas* cv);

//A draw call is defined as having one of three possible colormodes: set, blend or substract alpha.
//set is the basic one, the pixel is either set with full alpha, or nothing is set at all.
//blend will take the alpha value of the source color and use it to interpolate with the destination color (slower)
//substract alpha will completely ignore colors. It just substracts the alpha of the source to the alpha of the destination.
//	negative values get clamped to 0. This allows for various effects (2D lighting is the most important example)
void dhex_set_colormode(u8 colormode, dhex_canvas* cv);


//draw calls:
void dhex_clear_canvas(dhex_canvas* cv); //sets the color of every pixel to (0,0,0,0)
//it uses memset internally in order to do this fast.

void dhex_draw_rect(dhex_rgba color, dhex_canvas* cv, int x, int y, int w, int h);

void dhex_draw_image(dhex_image* mg, dhex_canvas* cv, int x, int y, int xOffset, int yOffset);
void dhex_draw_image_ex(dhex_image* mg, dhex_canvas* cv, int x, int y,
		int xOffset, int yOffset, float xscale, float yscale);
		
void dhex_draw_image_part(dhex_image* mg, dhex_canvas* cv, int x, int y, int xOffset, int yOffset,
	int xSection, int ySection, int wSection, int hSection);
	
void dhex_draw_sprite(dhex_sprite* spr, dhex_palette* pal, dhex_canvas* cv, int x, int y, int xOffset, int yOffset);
void dhex_draw_sprite_ex(dhex_sprite* spr, dhex_palette* pal, dhex_canvas* cv, int x, int y,
		int xOffset, int yOffset, float xscale, float yscale);
		
void dhex_draw_sprite_part(dhex_sprite* spr, dhex_palette* pal, dhex_canvas* cv, int x, int y, int xOffset, int yOffset,
	int xSection, int ySection, int wSection, int hSection);
	
//"Section" in the draw_<spr/img>_part functions refers to the rectangular section (relative to the image's pixels)
//that will be drawn. The picture location is drawn as if no pixels were skipped aka as if the full image was drawn 

#endif
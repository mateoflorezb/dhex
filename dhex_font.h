/*
	Usage:
	
	have a valid dhex_font initialized (no initialization function is provided, since it's
	kinda a lot of data). Every character is expected to have the same dimensions.
	If you don't want to deal with this, a default, NES-style font is provided
	by simply calling dhex_font_init().
	Once this function has been called, DHEX_FONT_DEFAULT is a valid dhex_font. 
	
	now simply call dhex_font_draw_text(...);

*/

#ifndef _dhex_font_h
#define _dhex_font_h

#include "dhex_draw.h"

enum _DHEX_FONT_CHARSET {
	
	DHEX_FONT_CHARSET_0 = 0,
	DHEX_FONT_CHARSET_1,
	DHEX_FONT_CHARSET_2,
	DHEX_FONT_CHARSET_3,
	DHEX_FONT_CHARSET_4,
	DHEX_FONT_CHARSET_5,
	DHEX_FONT_CHARSET_6,
	DHEX_FONT_CHARSET_7,
	DHEX_FONT_CHARSET_8,
	DHEX_FONT_CHARSET_9,
	
	DHEX_FONT_CHARSET_A,
	DHEX_FONT_CHARSET_B,
	DHEX_FONT_CHARSET_C,
	DHEX_FONT_CHARSET_D,
	DHEX_FONT_CHARSET_E,
	DHEX_FONT_CHARSET_F,
	DHEX_FONT_CHARSET_G,
	DHEX_FONT_CHARSET_H,
	DHEX_FONT_CHARSET_I,
	DHEX_FONT_CHARSET_J,
	DHEX_FONT_CHARSET_K,
	DHEX_FONT_CHARSET_L,
	DHEX_FONT_CHARSET_M,
	DHEX_FONT_CHARSET_N,
	DHEX_FONT_CHARSET_O,
	DHEX_FONT_CHARSET_P,
	DHEX_FONT_CHARSET_Q,
	DHEX_FONT_CHARSET_R,
	DHEX_FONT_CHARSET_S,
	DHEX_FONT_CHARSET_T,
	DHEX_FONT_CHARSET_U,
	DHEX_FONT_CHARSET_V,
	DHEX_FONT_CHARSET_W,
	DHEX_FONT_CHARSET_X,
	DHEX_FONT_CHARSET_Y,
	DHEX_FONT_CHARSET_Z,
	
	DHEX_FONT_CHARSET_UP,
	DHEX_FONT_CHARSET_RIGHT,
	DHEX_FONT_CHARSET_DOWN,
	DHEX_FONT_CHARSET_LEFT,
	
	DHEX_FONT_CHARSET_SIZE
	
};

typedef struct {
	dhex_sprite* charset[DHEX_FONT_CHARSET_SIZE]; 
} dhex_font;

extern dhex_font DHEX_FONT_DEFAULT;
void dhex_font_init(); //initialize this ^^^^

enum _DHEX_FONT_ALIGN {
	DHEX_FONT_ALIGN_CENTERED = 1,
	DHEX_FONT_ALIGN_LEFT //top left corner is the origin 
};

//will issue draw calls in the current layer with the current colormode
//use % as a escape char. %u will draw the "up" character, %r will draw the "right" character and so on 
void dhex_font_draw_text(dhex_font* font, dhex_palette* pal, dhex_canvas* cv, const char* txt, int align,
	int x, int y, float xscale, float yscale);

//sadly, these file representations in RAM are bound to endianess.
//since these files where compiled to C in an x86 machine, these are
//little endian

extern unsigned char _DHEX_FONT_DEFAULT_0[72];
extern unsigned char _DHEX_FONT_DEFAULT_1[72];
extern unsigned char _DHEX_FONT_DEFAULT_2[72];
extern unsigned char _DHEX_FONT_DEFAULT_3[72];
extern unsigned char _DHEX_FONT_DEFAULT_4[72];
extern unsigned char _DHEX_FONT_DEFAULT_5[72];
extern unsigned char _DHEX_FONT_DEFAULT_6[72];
extern unsigned char _DHEX_FONT_DEFAULT_7[72];
extern unsigned char _DHEX_FONT_DEFAULT_8[72];
extern unsigned char _DHEX_FONT_DEFAULT_9[72];

extern unsigned char _DHEX_FONT_DEFAULT_A[72];
extern unsigned char _DHEX_FONT_DEFAULT_B[72];
extern unsigned char _DHEX_FONT_DEFAULT_C[72];
extern unsigned char _DHEX_FONT_DEFAULT_D[72];
extern unsigned char _DHEX_FONT_DEFAULT_E[72];
extern unsigned char _DHEX_FONT_DEFAULT_F[72];
extern unsigned char _DHEX_FONT_DEFAULT_G[72];
extern unsigned char _DHEX_FONT_DEFAULT_H[72];
extern unsigned char _DHEX_FONT_DEFAULT_I[72];
extern unsigned char _DHEX_FONT_DEFAULT_J[72];
extern unsigned char _DHEX_FONT_DEFAULT_K[72];
extern unsigned char _DHEX_FONT_DEFAULT_L[72];
extern unsigned char _DHEX_FONT_DEFAULT_M[72];
extern unsigned char _DHEX_FONT_DEFAULT_N[72];
extern unsigned char _DHEX_FONT_DEFAULT_O[72];
extern unsigned char _DHEX_FONT_DEFAULT_P[72];
extern unsigned char _DHEX_FONT_DEFAULT_Q[72];
extern unsigned char _DHEX_FONT_DEFAULT_R[72];
extern unsigned char _DHEX_FONT_DEFAULT_S[72];
extern unsigned char _DHEX_FONT_DEFAULT_T[72];
extern unsigned char _DHEX_FONT_DEFAULT_U[72];
extern unsigned char _DHEX_FONT_DEFAULT_V[72];
extern unsigned char _DHEX_FONT_DEFAULT_W[72];
extern unsigned char _DHEX_FONT_DEFAULT_X[72];
extern unsigned char _DHEX_FONT_DEFAULT_Y[72];
extern unsigned char _DHEX_FONT_DEFAULT_Z[72];

extern unsigned char _DHEX_FONT_DEFAULT_UP[72];
extern unsigned char _DHEX_FONT_DEFAULT_RIGHT[72];
extern unsigned char _DHEX_FONT_DEFAULT_DOWN[72];
extern unsigned char _DHEX_FONT_DEFAULT_LEFT[72];

#endif
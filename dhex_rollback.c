#include "SDL.h"
#include "SDL_net.h"
#include "dhex_rollback.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <sys/time.h>

#define rb_private ((_dhex_rollback_private*)(rb->_private))

enum _DHEX_ROLLBACK_PROTOCOL {
	_DHEX_ROLLBACK_MESSAGE_ACK = 13, //acks will act as header and the following bytes describe what is being ack-ed
	_DHEX_ROLLBACK_MESSAGE_JOIN_REQUEST,
	_DHEX_ROLLBACK_MESSAGE_SYNC_REQUEST,
	_DHEX_ROLLBACK_MESSAGE_FRAME_INPUT,
	_DHEX_ROLLBACK_MESSAGE_CUSTOM //custom user message
	
};

typedef struct {
	int64_t seconds; //since epoch
	int32_t ms; //relative to <seconds>
} _dhex_timestamp;

typedef struct {
	uint32_t input;
	int64_t tick_count;
	_dhex_timestamp time;
} _dhex_frame_netinfo;

static void _local_get_timestamp(_dhex_timestamp* ts) {
	struct timeval tv;
	gettimeofday(&tv, NULL);
	ts->seconds = tv.tv_sec;
	ts->ms = tv.tv_usec/1000;
}

//returns time difference in ms between A and B (as A - B, so it can return negatives)
static inline int64_t _local_time_difference_ms(_dhex_timestamp A, _dhex_timestamp B) {
	return (A.seconds * 1000 + (int64_t)A.ms) - (B.seconds * 1000 + (int64_t)B.ms);
}

typedef struct {
	IPaddress peer_ip;
	UDPsocket sock;
	UDPpacket *out, *in;
	
	_dhex_timestamp last_sync_attempt_time;
	_dhex_timestamp confirmed_sync_time;
	
} _dhex_rollback_private;

int dhex_rollback_init(void) {
	
	if(SDL_Init(0)==-1) {
		return -1;
	}
	if(SDLNet_Init()==-1) {
		return -1;
	}	
	
	return 0;
}

dhex_rollback* dhex_create_rollback(signed char type, uint16_t server_port, const char* server_ip,
	void* (*_clone_gamestate)(void*), void (*_free_gamestate)(void*), void (*_advance_gamestate)(void*, uint32_t, uint32_t) ) {
		
	dhex_rollback* rb = (dhex_rollback*)malloc(sizeof(dhex_rollback));
	rb->_private = (void*)malloc(sizeof(_dhex_rollback_private));
	rb->connection_status = DHEX_CONNECTION_NONE;
	rb->type = DHEX_ROLLBACK_SERVER;
	if (type == DHEX_ROLLBACK_CLIENT) rb->type = DHEX_ROLLBACK_CLIENT;
	rb->server_port = server_port;
	rb->user_message_len = 0;
	rb->queued_user_messages = clist_create(_dhex_custom_packet);
	rb->sync_ok = 0;
	rb->sync_attempts_ttl = 0;
	rb->tick_count = 0;
	rb->try_sync = 0;
	rb->lag_stop = 0;
	rb->stored_frame_netinfo = clist_create(_dhex_frame_netinfo);
	rb->pending_frame_acks = clist_create(_dhex_frame_netinfo);
	
	rb->_clone_gamestate = _clone_gamestate;
	rb->_free_gamestate = _free_gamestate;
	rb->_advance_gamestate = _advance_gamestate;
	
	for (int k = 0; k < DHEX_ROLLBACK_MAX_STORED_GAMESTATES; k++) {
		rb->gamestate_history[k] = NULL;
		rb->peer_input_history[k] = (_dhex_frame_input){0,0};
		rb->local_input_history[k] = 0;
	}
	
	rb_private->in  = SDLNet_AllocPacket(DHEX_NETWORK_BUFFER_SIZE + 1);
	rb_private->out = SDLNet_AllocPacket(DHEX_NETWORK_BUFFER_SIZE + 1);
	
	if (type == DHEX_ROLLBACK_CLIENT) {
		rb_private->sock = SDLNet_UDP_Open(0);
		SDLNet_ResolveHost(&(rb_private->peer_ip), server_ip, server_port);
		SDLNet_UDP_Bind(rb_private->sock, 0, &(rb_private->peer_ip));
	}
	else {
		rb_private->sock = SDLNet_UDP_Open(server_port);
		SDLNet_UDP_Unbind(rb_private->sock, 0);
	}
	
	return rb;
}

static void _dhex_rollback_free_gamestates(dhex_rollback* rb) {
	for (int k = 0; k < DHEX_ROLLBACK_MAX_STORED_GAMESTATES; k++) {
		if (rb->gamestate_history[k] == NULL) rb->_free_gamestate(rb->gamestate_history[k]);
		rb->gamestate_history[k] = NULL;
	}
}

void dhex_free_rollback(dhex_rollback* rb) {
	
	_dhex_rollback_free_gamestates(rb);
	
	clist_iterator it = clist_start(rb->queued_user_messages);
	while (it != NULL) {
		clist_iterator next = clist_next(it);
		free(clist_get(it, _dhex_custom_packet).data);
		clist_delete(it);
		it = next;
	}
	clist_free(rb->queued_user_messages);
	
	clist_free(rb->stored_frame_netinfo);
	clist_free(rb->pending_frame_acks);
	
	SDLNet_FreePacket(rb_private->in);
	SDLNet_FreePacket(rb_private->out);
	SDLNet_UDP_Close(rb_private->sock);
	free((void*)rb_private);
	free((void*)rb);
}

void dhex_rollback_set_initial_gamestate(dhex_rollback* rb, void* gs) {
	_dhex_rollback_free_gamestates(rb);
	rb->tick_count = 0;
	
	for (int k = 0; k < DHEX_ROLLBACK_MAX_STORED_GAMESTATES; k++) {
		rb->peer_input_history[k] = (_dhex_frame_input){0,0};
		rb->local_input_history[k] = 0;
	}
	
	rb->gamestate_history[0] = rb->_clone_gamestate(gs);
	rb->sync_ok = 0; //after setting initial state we are expected to proceed with peer sync  
	rb->try_sync = 0;
	rb->lag_stop = 0;
}

//wrapped this in a function to abstract implementation details 
//also probably makes app source more readable
void dhex_rollback_try_sync(dhex_rollback* rb) {
	rb->try_sync = 1;
}

void dhex_buffer_write(void* buffer, void* data, int nbytes, int pos_offset, char swapByteOrder) {
	for (int k = 0; k < nbytes; k++) {
		int data_i = k;
		if (swapByteOrder) data_i = (nbytes - 1) - k;
		((uint8_t*)buffer)[k + pos_offset] = ((uint8_t*)data)[data_i];
	}
}

void dhex_buffer_read(void* buffer, void* data, int nbytes, int pos_offset, char swapByteOrder) {
	for (int k = 0; k < nbytes; k++) {
		int data_i = k;
		if (swapByteOrder) data_i = (nbytes - 1) - k;
		 ((uint8_t*)data)[data_i] = ((uint8_t*)buffer)[k + pos_offset];
	}
}

void dhex_rollback_send_message(dhex_rollback* rb, void* data, int nbytes) {
	if (rb->connection_status == DHEX_CONNECTION_OK) {
		if (nbytes > 0 && nbytes < DHEX_NETWORK_BUFFER_SIZE) {
			rb_private->out->len = nbytes + 1; //+1 to account for header byte 
			rb_private->out->data[0] = _DHEX_ROLLBACK_MESSAGE_CUSTOM;
			memcpy(&rb_private->out->data[1], data, nbytes);
			SDLNet_UDP_Send(rb_private->sock, 0, rb_private->out);			
		}
	}
}

int dhex_rollback_recv_message(dhex_rollback* rb) {
	if (clist_size(rb->queued_user_messages) <= 0) return 0;
	
	clist_iterator first = clist_start(rb->queued_user_messages);
	_dhex_custom_packet pk = clist_get(first, _dhex_custom_packet);
	memcpy(rb->user_message, pk.data, pk.nbytes);
	rb->user_message_len = pk.nbytes;
	free(pk.data);
	clist_delete(first);	
	return 1;
}

int dhex_rollback_check_connection(dhex_rollback* rb) {
	if (rb->connection_status == DHEX_CONNECTION_OK) return 1;
	return 0;
}

int dhex_rollback_check_sync(dhex_rollback* rb) {
	_dhex_timestamp current;
	_local_get_timestamp(&current);
	if (rb->sync_ok > 0 && _local_time_difference_ms(rb_private->confirmed_sync_time, current) <= 0) {
		return 1;
	}
	
	return 0;
}

//if more than DHEX_ROLLBACK_MAX_STORED_GAMESTATES frames have been guessed,
//do not step gamestate and just wait for ALL of the missing messages to arrive
void dhex_rollback_step_game(dhex_rollback* rb, uint32_t local_input_state) {
	
	if (!dhex_rollback_check_sync(rb)) return;
	
	char required_rollback = 0; //bool;
	int rollback_start_index = 0; //if <required_rollback>, from what array index (for gamestate history) do we start the rollback?
	
	//"tick count" but for the oldest stored frame 
	int64_t past_count = rb->tick_count - ( DHEX_ROLLBACK_MAX_STORED_GAMESTATES - 1 );
	
	//check what info from the peer's inputs we have stored 
	
	clist_iterator it = clist_start(rb->stored_frame_netinfo);
	while (it != NULL) {
		clist_iterator next = clist_next(it);
		
		_dhex_frame_netinfo info;
		memcpy(&info, it->data, sizeof(_dhex_frame_netinfo));
		
		//make sure we aren't reading a packet from the future 
		if (info.tick_count <= rb->tick_count) {
			
			//if the current frame info is relevant to the input history
			if (info.tick_count >= past_count) {
				
				//index relative to the gamestate history array 
				int relative_index = rb->tick_count - info.tick_count;
				
				if (!rb->peer_input_history[relative_index].confirmed) {
					
					rb->peer_input_history[relative_index].confirmed = 1;
					if (rb->peer_input_history[relative_index].input != info.input) {
						required_rollback = 1;
						rb->peer_input_history[relative_index].input = info.input;
						
						//so that only the furthest rollback starting index is defined
						if (relative_index > rollback_start_index) {rollback_start_index = relative_index;}
					}
				}
			}
			
			clist_delete(it);
		}
		
		it = next;
	}
	
	if (required_rollback) {
		
		//1. update peer input history (some predictions may be unaccurate after detecting a required_rollback)
		for (int k = 0; k < DHEX_ROLLBACK_MAX_STORED_GAMESTATES; k++) {
			if (!rb->peer_input_history[k].confirmed) {
				
				rb->peer_input_history[k].input = rb->peer_input_history[DHEX_ROLLBACK_MAX_STORED_GAMESTATES - 1].input;
				for (int sk = k+1; sk < DHEX_ROLLBACK_MAX_STORED_GAMESTATES; sk++) {
					if (rb->peer_input_history[sk].confirmed) {
						rb->peer_input_history[k].input = rb->peer_input_history[sk].input;
						break;
					}
				}
			}
		}
		//////////////////////////
		
		//2. free every inaccurate gamestate and advance (this is the actual rollbacking)
		for (int k = rollback_start_index - 1; k >= 0 ; k--) {
			
			rb->_free_gamestate( rb->gamestate_history[k] );
			rb->gamestate_history[k] = rb->_clone_gamestate( rb->gamestate_history[k + 1] );
			
			uint32_t p1,p2;
			
			if (rb->type == DHEX_ROLLBACK_CLIENT) {
				p1 = rb->peer_input_history[k].input;
				p2 = rb->local_input_history[k];
			}
			else {
				p2 = rb->peer_input_history[k].input;
				p1 = rb->local_input_history[k];
			}
			
			rb->_advance_gamestate(rb->gamestate_history[0], p1, p2);
		}
		
	}
	
	//check if we can continue after a lagstop. Resuming after lagstop will only 
	//happen if every stored gamestate received confirmation 
	if (rb->lag_stop) {
		int good = 1;
		for (int k = 0; k < DHEX_ROLLBACK_MAX_STORED_GAMESTATES; k++) {
			if (rb->peer_input_history[k].confirmed == 0) {
				good = 0;
				break;
			}
		}
		
		if (good) rb->lag_stop = 0;
	}
	
	//a lag stop will be notified if the DHEX_ROLLBACK_MAX_STORED_GAMESTATES'th frame before the current one is guessed 
	if (past_count >= 0 && rb->peer_input_history[DHEX_ROLLBACK_MAX_STORED_GAMESTATES - 1].confirmed == 0) {
		rb->lag_stop = 1;
	}
	
	//if no lagstop is in place, actually step state
	if (rb->lag_stop == 0) {
		
		//0. figure out inputs 
		uint32_t p1, p2;
		//peer input is the last known input for the peer 
		uint32_t peer_input = rb->peer_input_history[DHEX_ROLLBACK_MAX_STORED_GAMESTATES - 1].input;
		for (int k = 0; k < DHEX_ROLLBACK_MAX_STORED_GAMESTATES - 1; k++) {
			if (rb->peer_input_history[k].confirmed) {
				peer_input = rb->peer_input_history[k].input;
				break;
			}
		}
		
		if (rb->type == DHEX_ROLLBACK_CLIENT) {
			p1 = peer_input;
			p2 = local_input_state;
		}
		else {
			p2 = peer_input;
			p1 = local_input_state;			
		}
		
		rb->local_input_history[0] = local_input_state;
		rb->peer_input_history[0].input = peer_input;
		
		//1. free oldest state 
		if (past_count >= 0 && rb->gamestate_history[DHEX_ROLLBACK_MAX_STORED_GAMESTATES - 1] != NULL) {
			rb->_free_gamestate(rb->gamestate_history[DHEX_ROLLBACK_MAX_STORED_GAMESTATES - 1]);
		}
		//2. drag whole array 
		for (int k = DHEX_ROLLBACK_MAX_STORED_GAMESTATES - 1; k > 0; k--) {
			rb->gamestate_history[k] = rb->gamestate_history[k - 1];
			rb->local_input_history[k] = rb->local_input_history[k - 1];
			rb->peer_input_history[k] = rb->peer_input_history[k - 1];
		}
		
		//3. clone newest and advance
		rb->gamestate_history[0] = rb->_clone_gamestate( rb->gamestate_history[1] );
		rb->_advance_gamestate(rb->gamestate_history[0], p1, p2);
		rb->peer_input_history[0].confirmed = 0;
		
		//now, the gamestate in gamestate_history[0] is the state that the game was left in 
		//at the end of the current rb->tick_count and is also the exact same state that the frame
		//will start on for the next tick.
		//Basically this is probably what should get rendered 
		
		//4. add pending ack 
		_dhex_frame_netinfo new_pending_ack;
		new_pending_ack.input = local_input_state;
		new_pending_ack.tick_count = rb->tick_count;
		_local_get_timestamp(&new_pending_ack.time);
		
		clist_push(rb->pending_frame_acks, &new_pending_ack);
	
		rb->tick_count++;
	}
	
	dhex_rollback_update_network(rb);
}

void dhex_rollback_update_network(dhex_rollback* rb) {
	
	if (rb->type == DHEX_ROLLBACK_CLIENT) {
		
		//if no connection, send join request 
		if (rb->connection_status == DHEX_CONNECTION_NONE) {
			rb_private->out->len = 1;
			rb_private->out->data[0] = _DHEX_ROLLBACK_MESSAGE_JOIN_REQUEST;
			SDLNet_UDP_Send(rb_private->sock, 0, rb_private->out);
			//printf("Sent join request to server\n");
		}
	}
	else { //server side 
		
		if (rb->sync_ok == 0 && rb->connection_status == DHEX_CONNECTION_OK && rb->try_sync == 1) {
			if (rb->sync_attempts_ttl <= 0) {
				rb->sync_attempts_ttl = DHEX_NETWORK_SYNC_ATTEMPTS + 1;
				_local_get_timestamp(&rb_private->last_sync_attempt_time);
				//desired sync time is 3 seconds after sync attempts start 
				rb_private->last_sync_attempt_time.seconds += 3;
			}
			rb->sync_attempts_ttl--;
			
			rb_private->out->data[0] = _DHEX_ROLLBACK_MESSAGE_SYNC_REQUEST;
			int pos = 1;
			
			dhex_buffer_write(rb_private->out->data, &rb_private->last_sync_attempt_time.seconds,
				sizeof(int64_t), pos, DHEX_NETWORK_SWAP_ORDER);
				
			pos += sizeof(int64_t);
			dhex_buffer_write(rb_private->out->data, &rb_private->last_sync_attempt_time.ms,
				sizeof(int32_t), pos, DHEX_NETWORK_SWAP_ORDER);

			rb_private->out->len = 1 + sizeof(int64_t) + sizeof(int32_t);
			SDLNet_UDP_Send(rb_private->sock, 0, rb_private->out);
			printf("Sent sync request to client for s=%d, ms=%d\n", (int)rb_private->last_sync_attempt_time.seconds,
				(int)rb_private->last_sync_attempt_time.ms);
		}
	}
	
	// read packets ////////////////////
	while (SDLNet_UDP_Recv(rb_private->sock, rb_private->in) > 0) {
		
		// read stuff specific to client/server side //////////////
		if (rb->type == DHEX_ROLLBACK_CLIENT) {
			switch(rb_private->in->data[0]) {
				case _DHEX_ROLLBACK_MESSAGE_ACK:
				{
					if (rb_private->in->len == 2) {
						
						//if server accepted join request 
						if (rb_private->in->data[1] == _DHEX_ROLLBACK_MESSAGE_JOIN_REQUEST) {
							rb->connection_status = DHEX_CONNECTION_OK;	
							//printf("Server accepted join request\n");
						}
					}
				}
				break;
				
				case _DHEX_ROLLBACK_MESSAGE_SYNC_REQUEST:
				{
					int pos = 1;
					_dhex_timestamp desired_sync_time;
					
					dhex_buffer_read(rb_private->in->data, &desired_sync_time.seconds,
						sizeof(int64_t), pos, DHEX_NETWORK_SWAP_ORDER);
						
					pos += sizeof(int64_t);
					
					dhex_buffer_read(rb_private->in->data, &desired_sync_time.ms,
						sizeof(int32_t), pos, DHEX_NETWORK_SWAP_ORDER);
						
					//printf("Got sync request from server for s=%d, ms=%d\n", desired_sync_time.seconds, desired_sync_time.ms);

					_dhex_timestamp current_time;
					_local_get_timestamp(&current_time);
					
					if (current_time.seconds < desired_sync_time.seconds || 
						(
						current_time.seconds == desired_sync_time.seconds &&
						current_time.ms 	 <  desired_sync_time.ms
						)
					) { //if current time is before desired sync time 
					
						rb_private->out->data[0] = _DHEX_ROLLBACK_MESSAGE_ACK;
						rb_private->out->data[1] = _DHEX_ROLLBACK_MESSAGE_SYNC_REQUEST;
						int pos = 2;
						
						dhex_buffer_write(rb_private->out->data, &desired_sync_time.seconds,
							sizeof(int64_t), pos, DHEX_NETWORK_SWAP_ORDER);
							
						pos += sizeof(int64_t);
						dhex_buffer_write(rb_private->out->data, &desired_sync_time.ms,
							sizeof(int32_t), pos, DHEX_NETWORK_SWAP_ORDER);

						rb_private->out->len = 2 + sizeof(int64_t) + sizeof(int32_t);
						for (int k = 0; k < 3; k++) { //send the packet 3 times juust in case of packet loss 
							SDLNet_UDP_Send(rb_private->sock, 0, rb_private->out);	
							printf("Sent sync ack to server for s=%d, ms=%d\n", (int)desired_sync_time.seconds,
								(int)desired_sync_time.ms);
						}

						rb->sync_ok = 1;
						memcpy(&(rb_private->confirmed_sync_time), &desired_sync_time, sizeof(_dhex_timestamp) );
					}
				}
				break;
				
				default: break;
			}
		}
		else { //server side 
			switch(rb_private->in->data[0]) {
				
				case _DHEX_ROLLBACK_MESSAGE_ACK:
				{
					if (rb_private->in->data[1] == _DHEX_ROLLBACK_MESSAGE_SYNC_REQUEST) {
						_dhex_timestamp ack_time;
						int pos = 2;
						
						dhex_buffer_read(rb_private->in->data, &ack_time.seconds,
							sizeof(int64_t), pos, DHEX_NETWORK_SWAP_ORDER);
							
						pos += sizeof(int64_t);
						dhex_buffer_read(rb_private->in->data, &ack_time.ms,
							sizeof(int32_t), pos, DHEX_NETWORK_SWAP_ORDER);
						
						//if the acknoledged time is the same as the last attempt time 
						if (ack_time.seconds == rb_private->last_sync_attempt_time.seconds &&
							ack_time.ms      == rb_private->last_sync_attempt_time.ms) {
							rb->sync_ok = 1;
							memcpy(&(rb_private->confirmed_sync_time), &ack_time, sizeof(_dhex_timestamp) );
							printf("Confirmed sync ack from client\n");							
						}
						else {
							printf("Bad sync time for ack at s=%d, ms=%d\n",(int)ack_time.seconds, (int)ack_time.ms);			
						}
					}
				}
				break;
				
				case _DHEX_ROLLBACK_MESSAGE_JOIN_REQUEST:
					if (rb->connection_status == DHEX_CONNECTION_NONE) {
						memcpy(&rb_private->peer_ip, &rb_private->in->address, sizeof(IPaddress));
						/* bind client address to channel 0 */
						SDLNet_UDP_Bind(rb_private->sock, 0, &rb_private->peer_ip);
						rb->connection_status = DHEX_CONNECTION_OK;
						//printf("Accepted join request from client\n");
					}
					
					//this must NOT be an else, because:
					//case 1. connection was == none and we need to execute *both* 
					//case 2. connection was == ok and we only need to execute the second one 
					if (rb->connection_status == DHEX_CONNECTION_OK) {
						
						rb_private->out->len = 2;
						rb_private->out->data[0] = _DHEX_ROLLBACK_MESSAGE_ACK;
						rb_private->out->data[1] = _DHEX_ROLLBACK_MESSAGE_JOIN_REQUEST;
						SDLNet_UDP_Send(rb_private->sock, 0, rb_private->out);	
						//printf("Sent join request ack\n");
					}
					
				break;
				
				default: break;			
			}
		}
		
		// read general stuff that both client/server side have the same behaviour to 
		switch(rb_private->in->data[0]) {
			case _DHEX_ROLLBACK_MESSAGE_CUSTOM:
			{
				if (rb->connection_status == DHEX_CONNECTION_OK) {
					
					//if max messages are already queued, pop first one in the list (since the one we are going
					//to add will be pushed at the end, this way we keep the most recent messages)
					if (clist_size(rb->queued_user_messages) > DHEX_NETWORK_MAX_STORED_CUSTOM_MESSAGES) {
						clist_iterator first = clist_start(rb->queued_user_messages); //get first node address 
						free(clist_get(first, _dhex_custom_packet).data); //cast to actual type to free internal member 
						clist_delete(first); //delete from list 
					}
					
					int nbytes = rb_private->in->len - 1; //-1 since we will not be writing header byte here 
					_dhex_custom_packet p; 
					p.nbytes = nbytes;
					p.data = malloc(nbytes);
					memcpy(p.data, &rb_private->in->data[1], nbytes);
					clist_push(rb->queued_user_messages, &p);
				}
			}
			break;
			
			case _DHEX_ROLLBACK_MESSAGE_FRAME_INPUT:
			{
				_dhex_frame_netinfo info;
				
				int pos = 1;
				
				dhex_buffer_read(rb_private->in->data, &info.input,
					sizeof(uint32_t), pos, DHEX_NETWORK_SWAP_ORDER);
				pos += sizeof(uint32_t);
				
				dhex_buffer_read(rb_private->in->data, &info.tick_count,
					sizeof(int64_t), pos, DHEX_NETWORK_SWAP_ORDER);
				pos += sizeof(int64_t);
				
				dhex_buffer_read(rb_private->in->data, &info.time.seconds,
					sizeof(int64_t), pos, DHEX_NETWORK_SWAP_ORDER);
				pos += sizeof(int64_t);
				
				dhex_buffer_read(rb_private->in->data, &info.time.ms,
					sizeof(int32_t), pos, DHEX_NETWORK_SWAP_ORDER);
					
				char repeated = 0;
					
				//check if we hadn't this frame info stored already 
				clist_iterator it = clist_start(rb->stored_frame_netinfo);
				while (it != NULL) {
					clist_iterator next = clist_next(it);
					_dhex_frame_netinfo stored;
					memcpy(&stored, it->data, sizeof(_dhex_frame_netinfo));
					
					if (stored.tick_count == info.tick_count) {
						repeated = 1;
						break;
					}
					
					it = next;
				}
				
				if (!repeated) {
					clist_push(rb->stored_frame_netinfo, &info);
				}
				
				rb_private->out->data[0] = _DHEX_ROLLBACK_MESSAGE_ACK;
				rb_private->out->data[1] = _DHEX_ROLLBACK_MESSAGE_FRAME_INPUT;
				
				dhex_buffer_write(rb_private->out->data, &info.tick_count,
				sizeof(int64_t), 2, DHEX_NETWORK_SWAP_ORDER);
				
				rb_private->out->len = 2 + sizeof(int64_t);
				
				SDLNet_UDP_Send(rb_private->sock, 0, rb_private->out);				
			}
			break;
			
			case _DHEX_ROLLBACK_MESSAGE_ACK:
			{
				if (rb_private->in->data[1] == _DHEX_ROLLBACK_MESSAGE_FRAME_INPUT) {
					int64_t tick;
					dhex_buffer_read(rb_private->in->data, &tick,
					sizeof(int64_t), 2, DHEX_NETWORK_SWAP_ORDER);

					//check if this ack was pending
					clist_iterator it = clist_start(rb->pending_frame_acks);
					while (it != NULL) {
						clist_iterator next = clist_next(it);
						_dhex_frame_netinfo pend;
						memcpy(&pend, it->data, sizeof(_dhex_frame_netinfo));
						
						if (pend.tick_count == tick) {
							clist_delete(it);
							break;
						}
						
						it = next;
					}					
				}
			}
			break;
			
			default: break;
		}
	} /// WHILE READING PACKETS /////////////
	
	// send frame netinfo ////////////////////
	if (dhex_rollback_check_sync(rb)) {
		clist_iterator it = clist_start(rb->pending_frame_acks);
		while (it != NULL) {
			
			_dhex_frame_netinfo info;
			memcpy(&info, it->data, sizeof(_dhex_frame_netinfo));
			
			rb_private->out->data[0] = _DHEX_ROLLBACK_MESSAGE_FRAME_INPUT;
			int pos = 1;
			
			dhex_buffer_write(rb_private->out->data, &info.input,
				sizeof(uint32_t), pos, DHEX_NETWORK_SWAP_ORDER);
			pos += sizeof(uint32_t);
			
			dhex_buffer_write(rb_private->out->data, &info.tick_count,
				sizeof(int64_t), pos, DHEX_NETWORK_SWAP_ORDER);
			pos += sizeof(int64_t);
			
			dhex_buffer_write(rb_private->out->data, &info.time.seconds,
				sizeof(int64_t), pos, DHEX_NETWORK_SWAP_ORDER);
			pos += sizeof(int64_t);
			
			dhex_buffer_write(rb_private->out->data, &info.time.ms,
				sizeof(int32_t), pos, DHEX_NETWORK_SWAP_ORDER);
			pos += sizeof(int32_t);

			rb_private->out->len = pos;
			SDLNet_UDP_Send(rb_private->sock, 0, rb_private->out);
			
			it = clist_next(it);
		}
	}
}
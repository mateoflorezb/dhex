#ifndef _dhex_rollback_h
#define _dhex_rollback_h

#include <stdint.h>
#include "clist.h"

#define DHEX_NETWORK_BUFFER_SIZE 1024
#define DHEX_NETWORK_MAX_STORED_CUSTOM_MESSAGES 20
#define DHEX_NETWORK_SYNC_ATTEMPTS 90
#define DHEX_NETWORK_SWAP_ORDER 0

//this would also be the "max rollback", aka a rollback cannot go back more than
//this number of frames 
#define DHEX_ROLLBACK_MAX_STORED_GAMESTATES 20

//each frame input confirmation data is stored in a queue. This is it's max size 
#define DHEX_NETWORK_MAX_STORED_FRAME_NETINFO 35

#define DHEX_NETWORK_MAX_STORED_PENDING_FRAME_ACKS 25

//basically to initialize sdl_net 
//returns -1 on errors
int dhex_rollback_init(void);

enum _DHEX_ROLLBACK_CONNECTION_STATUS {
	DHEX_CONNECTION_NONE = 1,
	DHEX_CONNECTION_OK,
	DHEX_CONNECTION_LOST
};

typedef struct {
	int nbytes;
	uint8_t* data;
} _dhex_custom_packet;

typedef struct {
	//bool. For local inputs, set to 1 if we already got network ack. For peer inputs, set to 1
	//if we already got network confirmation (otherwise set to 0, it means it was a guessed input)
	uint8_t confirmed; 
	uint32_t input;
} _dhex_frame_input;

typedef struct {
	int8_t type; //server or client 
	int8_t connection_status; //no connection yet, connection OK or disconnected
	uint16_t server_port;
	int64_t tick_count; //for game logic 
	int8_t sync_ok; //char bool. Set to true if already got timestamp confirmation for starting 
	int sync_attempts_ttl;
	uint8_t try_sync; //bool. If true and peers have yet to sync, sync request will be sent 
	
	//bool. set to true if DHEX_ROLLBACK_MAX_STORED_GAMESTATES number of frames have already been guessed and 
	//we need to stop advancing gamestate until we get confirmation for every guessed frame. 
	uint8_t lag_stop; 
	
	//to allow for the application source code (aka, the user) to send 
	//custom messages between peers (to implement things like chat for instance)
	//check dhex_rollback_<send/recv>_message(...) for more details
	uint8_t user_message[DHEX_NETWORK_BUFFER_SIZE]; 
	clist* queued_user_messages;
	int user_message_len;
	
	//receives address of gamestate and returns a deep copy of it 
	void* (*_clone_gamestate)(void*);
	
	//receives address of gamestate and frees it 
	void  (*_free_gamestate)(void*);
	
	//receives address of gamestate with p1/p2 inputs encoded as uint32_t and advances gamestate 1 frame 
	void (*_advance_gamestate)(void*, uint32_t, uint32_t);
	
	//for these, index 0 is the current gamestate, index 1 is 1 frame before and so on.
	//these are only modified by dhex_rollback_step_game()
	////////////////////////////////////////////////////////
	void* gamestate_history[DHEX_ROLLBACK_MAX_STORED_GAMESTATES]; //history of the initial state of each frame
	_dhex_frame_input peer_input_history[DHEX_ROLLBACK_MAX_STORED_GAMESTATES];
	uint32_t local_input_history[DHEX_ROLLBACK_MAX_STORED_GAMESTATES];
	////////////////////////////////////////////////////////
	
	//stored confirmations from the peer 
	//modified by dhex_rollback_step_game(...) (nodes are deleted here) and dhex_rollback_update_network() (nodes are added here)
	clist* stored_frame_netinfo;
	
	//modified by dhex_rollback_step_game(...) (nodes are added here) and dhex_rollback_update_network() (nodes are deleted here)
	clist* pending_frame_acks;
	
	void* _private; //implementation specific (basically I don't want to #include sdl_net here)
	
} dhex_rollback;

enum _DHEX_ROLLBACK_TYPES {
	DHEX_ROLLBACK_SERVER = 1,
	DHEX_ROLLBACK_CLIENT
};

//even though these are called "server" and "client", this is really more of a peer to peer model,
//it's just that "server" waits for a "client" to say "hey, I want to play!"
//type --> one of _DHEX_ROLLBACK_TYPES
//server_port --> must be specified by both server and client side
//server_ip --> must be specified exclusively by client side (pass anything for server side, it will get ignored)
dhex_rollback* dhex_create_rollback(signed char type, uint16_t server_port, const char* server_ip,
	void* (*_clone_gamestate)(void*), void (*_free_gamestate)(void*), void (*_advance_gamestate)(void*, uint32_t, uint32_t) );

void dhex_free_rollback(dhex_rollback* rb);

//gs is a gamestate to be cloned. Any previous stored stuff (previous gamestates,
//	frame counter, etc) will be freed and the rollback will wait for syncing with the peer 
void dhex_rollback_set_initial_gamestate(dhex_rollback* rb, void* gs);

//THIS WILL FLUSH EVERY RECEIVED UDP PACKET IN THE SOCKET
//(both sides) checks UDP packets and updates state accordingly
//(things like what frames' inputs do we have confirmation for and join requests).
//sends relevant packets.
//THIS FUNCTION IS EXPECTED TO BE CALLED AT 60HZ
void dhex_rollback_update_network(dhex_rollback* rb);

//send a custom message to the peer. Messages longer than
//DHEX_NETWORK_BUFFER_SIZE will be rejected and not sent.
//Messages will only be sent if connection_status == OK 
void dhex_rollback_send_message(dhex_rollback* rb, void* data, int nbytes);

//receive a custom message from the peer. Returns a positive int if a message 
//was received. 
//Messages will only be read if connection_status == OK 
//If more than one user message was received, subsequent calls of this function will 
//still return positive, so to process all messages one should do:
/*
	while (dhex_rollback_recv_message(rb) > 0) {
		//process message here.
		//the message itself is in rb->user_message
		//and the length of the message is defined by rb->user_message_len
	}

*/
int dhex_rollback_recv_message(dhex_rollback* rb);

//Following two functions aren't safe, make sure that the buffer size is correct.
/*
writes data into a buffer. If (swapByteOrder), then write the data into the buffer from
	back to front.

nbytes is how many bytes from <data> to read
pos_offset is how many bytes to skip from the start of the buffer
*/
void dhex_buffer_write(void* buffer, void* data, int nbytes, int pos_offset, char swapByteOrder);
/*
basically the same, just that this time we are taking what's into the buffer and putting it into 
	the data. 
*/
void dhex_buffer_read(void* buffer, void* data, int nbytes, int pos_offset, char swapByteOrder);

//Example of why these two functions are useful:
/*

	//if we are compiling dhex for a little endian architecture
	#define NETWORK_SWAP_ORDER 0
	//it we are not, we would do this
	#define NETWORK_SWAP_ORDER 1

	//now let's say that we wanted to write an int64 at pos 2 in the buffer

	int64 myInt = 10;
	dhex_buffer_write(out, &myInt, sizeof(int64), 2, NETWORK_SWAP_ORDER);


	//the peer would read it like:
	int64 myInt;
	dhex_buffer_read(in, &myInt, sizeof(int64), 2, NETWORK_SWAP_ORDER);

*/

int dhex_rollback_check_connection(dhex_rollback* rb); //returns 0 if connection status is none 
int dhex_rollback_check_sync(dhex_rollback* rb); //returns 0 if syncing is yet to be done 

void dhex_rollback_try_sync(dhex_rollback* rb); //raises the try sync flag. When this is raised sync attempts are sent 

//if more than DHEX_ROLLBACK_MAX_STORED_GAMESTATES frames have been guessed,
//do not step gamestate and just wait for ALL of the missing messages to arrive
//This should be called each frame after calling dhex_rollback_update_network();
void dhex_rollback_step_game(dhex_rollback* rb, uint32_t local_input_state);

#endif
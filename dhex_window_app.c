#include "dhex_window_app.h"
#include "SDL.h"
#include "SDL_opengl.h"
#include "SDL_mixer.h"
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>

//implementation for global var 
dhex_timestamp _DHEX_PROGRAM_EPOCH_START; 

int64_t _DHEX_SET_TIMER_MICROS_RECORDED = 0;

static inline int iclamp(int value, int min, int max) {
	if (value < min) return min;
	if (value > max) return max;
	return value;
}

typedef struct {
	int32_t device_port[__DHEX_MAX_DEVICES]; //SDL joy ids
	SDL_Window* window;
	SDL_GLContext context;
} _dhex_app_private;

//////////////////////////////////////////////////////////////////////
static void local_dhex_refresh_devices(dhex_app* app) {
	
	//set every port to null
	memset(  ((_dhex_app_private*)app->private)->device_port  ,
				0xFF, sizeof(  ((_dhex_app_private*)app->private)->device_port  ));

    int devices = SDL_NumJoysticks();
    if (devices < 0) {
		if (app->hide_out == 0)
        printf("SDL: %s\n", SDL_GetError() );
        return;
    }
	
	if (app->hide_out == 0)
    printf("Detected %d joystick(s)\n", devices);

    if (devices == 0) return;
	
    for (uint8_t j = 0; j < (int)fmin(devices, __DHEX_MAX_DEVICES); j++) {
        SDL_Joystick* Joy = SDL_JoystickOpen(j);
		((_dhex_app_private*)app->private)->device_port[j] = SDL_JoystickInstanceID(Joy);
    }

}

//////////////////////////////////////////////////////////////////////
dhex_app* dhex_create_app(const char* display_name, int w, int h, double aspect_ratio, int max_players, unsigned char flags) {
	
	{
	struct timeval tv;
	gettimeofday(&tv, NULL);
	_DHEX_PROGRAM_EPOCH_START.seconds = tv.tv_sec;
	_DHEX_PROGRAM_EPOCH_START.microseconds = tv.tv_usec;
	}
	
	_dhex_app_private* private = malloc(sizeof(_dhex_app_private));
	
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK | SDL_INIT_AUDIO) < 0) {
        printf("SDL_Init: %s\n", SDL_GetError());
        free(private);
        return NULL;
    }
	
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);
	
	//to account for games with non square pixels
	int win_h;
	win_h = h;
	
	if ((flags & DHEX_APP_ASPECT_RATIO_CORRECTION) != 0) {
		win_h = floor((double)w / aspect_ratio);
	}

    private->window = SDL_CreateWindow(display_name,
                               SDL_WINDOWPOS_UNDEFINED,
                               SDL_WINDOWPOS_UNDEFINED,
                               w,
                               win_h,
                               SDL_WINDOW_RESIZABLE | SDL_WINDOW_OPENGL);

    if (private->window == NULL) {
        printf("SDL_CreateWindow: %s\n", SDL_GetError());
        free(private);
        return NULL;
    }
	
	private->context = SDL_GL_CreateContext(private->window);
	
	if (private->context == NULL) {
        printf("SDL_GL_CreateContext: %s\n", SDL_GetError());
        free(private);
        return NULL;		
	}
	
    glViewport(0, 0, w, win_h);
    SDL_GL_SetSwapInterval(0);
	
	dhex_app* new_app = malloc(sizeof(dhex_app));
	new_app->main_canvas = dhex_create_canvas(w, h);
	new_app->quit = 0;
	new_app->clear_window_queued = 0;
	new_app->scaler = DHEX_SCALER_RATIO;
	new_app->input.any_vk = 0;
	new_app->input.last_vk = (dhex_vk){.type = DHEX_INPUT_HW_NONE};
	new_app->input.max_players = max_players;
	new_app->input.axis_deadzone = 27000;
	new_app->aspect_ratio = aspect_ratio;
	
	if ((flags & DHEX_APP_ASPECT_RATIO_CORRECTION) != 0) {
		new_app->ratio_correction_enabled = 1;
	}
	else {
		new_app->ratio_correction_enabled = 0;
	}
	
	for (int btn = 0; btn < DHEX_INPUT_MAX; btn++) {
		new_app->input.config[btn] = malloc(sizeof(dhex_vk)*max_players);
		new_app->input.state[btn] = malloc(sizeof(int)*max_players);
		new_app->input.state_prev[btn] = malloc(sizeof(int)*max_players);
		for (int p = 0; p < max_players; p++) {
			new_app->input.config[btn][p] = (dhex_vk){.type = DHEX_INPUT_HW_NONE};
			new_app->input.state[btn][p] = 0;
			new_app->input.state_prev[btn][p] = 0;
		}
	}
	
	new_app->private = (void*)private;
	local_dhex_refresh_devices(new_app);
	
	new_app->hide_out = 0;
	if ((flags & DHEX_APP_HIDE_STDOUT) != 0) 
		new_app->hide_out = 1;
		
	
	signed char sfx_error = 0;
	if ((flags & DHEX_APP_SFX) != 0)
	for (int o = 0; o <= 0; o++) { //just to be able to use break;
		
		{
			uint32_t flags = MIX_INIT_OGG;
			if ((Mix_Init(MIX_INIT_OGG) & flags) != flags) {
				printf("Mix_Init: %s\n", Mix_GetError() );
				sfx_error = -1;
				break;
			}
		}

		//default frequency is 22050 samples per second
		//default format is signed 16 bit
		//2 channels (stereo, nothing to do with mixing channels)
		//so, bytes per second of sfx = 22050 * 2 (bytes per sample) * 2 (channels)
		//so bps = 88200
		//bytes per frame (at 60hz) = 1470
		if (Mix_OpenAudio(DHEX_SFX_FREQUENCY, AUDIO_S16SYS, 2, DHEX_SFX_CHUNKSIZE) < 0) {
			printf("Mix_OpenAudio: %s\n", Mix_GetError() );
			sfx_error = -1;
			break;
		}
		
		Mix_AllocateChannels(DHEX_SFX_CHANNELS);

	}
	
	if (sfx_error < 0) {
		dhex_free_app(new_app);
		return NULL;
	}
	
	return new_app;
}


//////////////////////////////////////////////////////////////////////
void dhex_free_app(dhex_app* app) {
	_dhex_app_private* pv = (_dhex_app_private*)app->private;
	SDL_DestroyWindow(pv->window);
	free(app->private);
	dhex_free_canvas(app->main_canvas);
	
	for (int btn = 0; btn < DHEX_INPUT_MAX; btn++) {
		free(app->input.config[btn]);
		free(app->input.state[btn]);
		free(app->input.state_prev[btn]);
	}	
	
	free(app);
	Mix_Quit();
	SDL_Quit();
}


//////////////////////////////////////////////////////////////////////
void dhex_refresh_window(dhex_app* app) {
	
	int w,h;
	w = app->main_canvas->pixelbuffer.w;
	h = app->main_canvas->pixelbuffer.h;
	dhex_rgba* pixeldata = app->main_canvas->pixelbuffer.pixeldata;
	SDL_Window* window = ((_dhex_app_private*)app->private)->window;
	
	if (app->clear_window_queued != 0) {
		glClearColor (0.0, 0.0, 0.0, 1.0);
		glClear (GL_COLOR_BUFFER_BIT);
		app->clear_window_queued = 0;
	}
	
	int win_x, win_y;
	SDL_GetWindowSize(window, &win_x, &win_y);
	
	if (app->scaler == DHEX_SCALER_STRETCH) {
	
		float xscale = (float)win_x / (float)w;
		float yscale = (float)win_y / (float)h;
		
		glRasterPos2i(-1, 1);
		glPixelZoom(xscale, -yscale);
		glDrawPixels(w, h, GL_RGBA, GL_UNSIGNED_BYTE, (const void*) pixeldata);
	}
	else {
		int viewport_w, viewport_h;
		float window_ratio = (float)win_x / (float)win_y;
		float aratio;
		float rpx = -1, rpy = 1;
		
		if (app->ratio_correction_enabled == 0) 
			aratio = (float)app->main_canvas->pixelbuffer.w / (float)app->main_canvas->pixelbuffer.h;
		else
			aratio = app->aspect_ratio;
		
		if (window_ratio > aratio) {
			viewport_h = win_y;
			viewport_w = aratio * win_y;
			float unused_ptg = (float)(win_x - viewport_w) / (float)win_x;
			rpx = 2.0 * (unused_ptg / 2.0) - 1.0;
		}
		else {
			viewport_w = win_x;
			viewport_h = win_x / aratio;	
			float unused_ptg = (float)(win_y - viewport_h) / (float)win_y;
			rpy = -2.0 * (unused_ptg / 2.0) + 1.0;			
		}
		
		float xscale = (float)viewport_w / (float)w;
		float yscale = (float)viewport_h / (float)h;	

		glRasterPos2f(rpx, rpy);
		glPixelZoom(xscale, -yscale);
		glDrawPixels(w, h, GL_RGBA, GL_UNSIGNED_BYTE, (const void*) pixeldata);		
		
	}
   
	SDL_GL_SwapWindow(window);
}


//////////////////////////////////////////////////////////////////////
int64_t dhex_get_micros(void) {
	
	struct timeval tv;
	gettimeofday(&tv, NULL);
	int64_t seconds_elapsed = (tv.tv_sec - _DHEX_PROGRAM_EPOCH_START.seconds);
	return (seconds_elapsed * 1000000) + (tv.tv_usec - _DHEX_PROGRAM_EPOCH_START.microseconds);
}

//////////////////////////////////////////////////////////////////////
void dhex_microsleep(int64_t micros) {
	if (micros > 0) {
		struct timespec req;
		req.tv_sec = 0;
		req.tv_nsec = micros * 1000;
		nanosleep(&req, NULL);
	}
}

void dhex_set_timer() {
	_DHEX_SET_TIMER_MICROS_RECORDED = dhex_get_micros();
}
void dhex_wait_timer(int64_t micros) {
	int64_t t = dhex_get_micros();
	dhex_microsleep(micros - (t - _DHEX_SET_TIMER_MICROS_RECORDED));
}

void dhex_set_scaler(dhex_app* app, char scaler) {
	app->scaler = scaler;
	app->clear_window_queued = 1;
}

void dhex_set_window_size(dhex_app* app, int w, int h) {
	SDL_SetWindowSize(((_dhex_app_private*)app->private)->window, w, h);
}

//////////////////////////////////////////////////////////////////////
void dhex_input_config_set(dhex_app* app, int button, int player, dhex_vk vk) {
	if (
		button < 0 || button >= DHEX_INPUT_MAX ||
		player < 0 || player >= app->input.max_players
	) {
		if (app->hide_out == 0)
		printf("Warning: attempted to call dhex_input_config_set(...) with bad arguments.\n");
		
	}
	else 
		app->input.config[button][player] = vk;
	
}


//////////////////////////////////////////////////////////////////////
void dhex_input_config_save(dhex_app* app, const char* filename) {
	
	FILE* f = fopen(filename, "wb");
	
	if (f != NULL) {
		
		fwrite(&app->input.max_players, sizeof(int), 1, f);
		
		for (int btn = 0; btn < DHEX_INPUT_MAX; btn++)
		for (int p = 0; p < app->input.max_players; p++) {
			
			fwrite(&app->input.config[btn][p], sizeof(dhex_vk), 1, f);
		}
	
		fclose(f);
	}
}


//////////////////////////////////////////////////////////////////////
void dhex_input_config_load(dhex_app* app, const char* filename) {
	
	FILE* f = fopen(filename, "rb");
	
	if (f != NULL) {
		
		int max_p;
		fread(&max_p, sizeof(int), 1, f);
		
		if (app->input.max_players == max_p) {
			
			app->input.max_players = max_p;
			for (int btn = 0; btn < DHEX_INPUT_MAX; btn++)
			for (int p = 0; p < app->input.max_players; p++) {
				
				fread(&app->input.config[btn][p], sizeof(dhex_vk), 1, f);
			}
		}
		else {
			if (app->hide_out == 0)
			printf("Warning: tried to load a bad input config file.\n");
			
		}
	
		fclose(f);
	}
	else {
		if (app->hide_out == 0)
		printf("Warning: tried to load non existent input config file.\n");
		
	}
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

void* dhex_load_sfx(const char* filename) {
	return (void*)(Mix_LoadWAV(filename));
}

void  dhex_free_sfx(void* sfx) {
	Mix_FreeChunk((Mix_Chunk*)sfx);	
}

void* dhex_load_music(const char* filename) {
	return (void*)Mix_LoadMUS(filename);
}

void  dhex_free_music(void* music) {
	Mix_FreeMusic((Mix_Music*) music);
}

int dhex_sfx_volume(void* sfx, int volume) {
	return Mix_VolumeChunk((Mix_Chunk*) sfx, volume);
}

int dhex_sfx_channel_volume(int channel, int volume) {
	return Mix_Volume(channel, volume);
}

int dhex_music_volume(int volume) {
	return Mix_VolumeMusic(volume);
}

int dhex_music_play(void *music, int loops) {
	return Mix_PlayMusic((Mix_Music*) music, loops);
}

void dhex_music_pause(void) {
	Mix_PauseMusic();
}

void dhex_music_resume(void) {
	Mix_ResumeMusic();
}

void dhex_music_halt(void) {
	Mix_HaltMusic();
}

void dhex_sfx_play(int channel, void* sfx, int64_t skip_bytes) {
	Mix_Chunk* chunk = (Mix_Chunk*)sfx;
    const uint8_t* original_abuf = chunk->abuf;
    uint32_t original_alen = chunk->alen;
	
	if (original_alen <= skip_bytes) return;
	
    chunk->abuf = (uint8_t*)(original_abuf+skip_bytes);
    chunk->alen = original_alen - skip_bytes;
    Mix_PlayChannel(channel, chunk, 0);

    chunk->abuf = (uint8_t*)original_abuf;
    chunk->alen = original_alen;
}

void dhex_sfx_halt_channel(int channel) {
	Mix_HaltChannel(channel);
}


//////////////////////////////////////////////////////////////////////
int dhex_check_vk_direct(dhex_app* app, dhex_vk vk) {
	
	switch(vk.type) {
		
		case DHEX_INPUT_HW_NONE: return 0; break;
		
		case DHEX_INPUT_HW_KEYBOARD:
		{
			const uint8_t* kbState = SDL_GetKeyboardState(NULL);
			return (kbState[vk.hw.keyboard.key]);
		}
		break;
		
		
		case DHEX_INPUT_HW_JOYBUTTON:
		{
			
		_dhex_app_private* pv = (_dhex_app_private*)app->private;
			
		if (pv->device_port[vk.hw.joybutton.device] == 0xFFFFFFFF) break;
		return (SDL_JoystickGetButton(
					SDL_JoystickFromInstanceID(pv->device_port[vk.hw.joybutton.device]),
					vk.hw.joybutton.button)  
				);

		}
		break;
		
		
		
        case DHEX_INPUT_HW_JOYAXIS:
        {
			_dhex_app_private* pv = (_dhex_app_private*)app->private;
            if (pv->device_port[vk.hw.joyaxis.device] == 0xFFFFFFFF) break;
            int16_t axis_val = SDL_JoystickGetAxis(
								SDL_JoystickFromInstanceID(pv->device_port[vk.hw.joyaxis.device]), 
								vk.hw.joyaxis.axis);
								
			signed char ax_dir = 1;
			if (axis_val < 0) ax_dir = -1;

            if ((abs(axis_val) > app->input.axis_deadzone) && (ax_dir == vk.hw.joyaxis.direction))
				return 1;

        }
        break;
		
		
		
        case DHEX_INPUT_HW_JOYHAT:
        {
			_dhex_app_private* pv = (_dhex_app_private*)app->private;
			if (pv->device_port[vk.hw.joyhat.device] == 0xFFFFFFFF) break;
			
            uint8_t hat = SDL_JoystickGetHat(
						  SDL_JoystickFromInstanceID(pv->device_port[vk.hw.joyhat.device]), vk.hw.joyhat.hat);
						  
			int _good = 0;
			 
			if ((hat == SDL_HAT_RIGHTUP || hat == SDL_HAT_RIGHTDOWN || hat == SDL_HAT_RIGHT)
				&& vk.hw.joyhat.direction == SDL_HAT_RIGHT) 
			   _good = 1;

			if ((hat == SDL_HAT_LEFTUP || hat == SDL_HAT_LEFTDOWN || hat == SDL_HAT_LEFT)
				&& vk.hw.joyhat.direction == SDL_HAT_LEFT) 
			   _good = 1;
			   
			if ((hat == SDL_HAT_LEFTUP || hat == SDL_HAT_UP || hat == SDL_HAT_RIGHTUP)
				&& vk.hw.joyhat.direction == SDL_HAT_UP) 
			   _good = 1;
			   
			if ((hat == SDL_HAT_LEFTDOWN || hat == SDL_HAT_RIGHTDOWN || hat == SDL_HAT_DOWN)
				&& vk.hw.joyhat.direction == SDL_HAT_DOWN) 
			   _good = 1;
			
		
            return _good;
        }
        break;
		
		
	}
	
	return 0;
}


int dhex_input_check(dhex_app* app, int input, int player) {
	return app->input.state[input][player];
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

static int local_dhex_find_joyport(dhex_app* app, SDL_JoystickID joyID, int8_t* port) {	
	_dhex_app_private* pv = (_dhex_app_private*)app->private;
    for (int i = 0; i < __DHEX_MAX_DEVICES; i++) {
        if (pv->device_port[i] == joyID) {
            *port = i;
            return 1;
        }
    }
    return 0;

}

void dhex_refresh_app(dhex_app* app) {
	
	app->input.mouse_pressed = 0;
	app->input.any_vk = 0;

	SDL_Event e;
	while (SDL_PollEvent(&e)) {
		
			if (e.type == SDL_JOYDEVICEADDED || e.type == SDL_JOYDEVICEREMOVED) {
				local_dhex_refresh_devices(app);
			}

			switch(e.type) {
				
				case SDL_WINDOWEVENT:
					if (e.window.event == SDL_WINDOWEVENT_RESIZED || e.window.event == SDL_WINDOWEVENT_SIZE_CHANGED) {
						
						SDL_Window* window = ((_dhex_app_private*)app->private)->window;
						int w,h;
						SDL_GetWindowSize(window, &w, &h);
						glViewport(0, 0, w, h);
						//SDL_Surface* ws = SDL_GetWindowSurface(window);
						//SDL_FillRect(ws, NULL, SDL_MapRGBA(ws->format,0,0,0,255));
						
					}
				break;
				
				case SDL_QUIT:
					app->quit = 1;
				break;

				case SDL_MOUSEBUTTONDOWN:
					if ((uint8_t)(e.button.button) == SDL_BUTTON_LEFT) {
						app->input.mouse_held = 1;
						app->input.mouse_pressed = 1;
					}
				break;

				case SDL_MOUSEBUTTONUP:
					if ((uint8_t)(e.button.button) == SDL_BUTTON_LEFT) {
						app->input.mouse_held = 0;
					}
				break;
				
				/////////////////////////////////////
				/////////////////////////////////////
				/////////////////////////////////////
				/////////////////////////////////////
				
				case SDL_KEYDOWN:
				{
					
					app->input.last_vk = (dhex_vk){
						
						.type = DHEX_INPUT_HW_KEYBOARD,
						.hw.keyboard = {
							.key = e.key.keysym.scancode
						} 
					};
					
					app->input.any_vk = 1;
				}
				break;

				case SDL_JOYBUTTONDOWN:
				{
					uint8_t port;
					if (local_dhex_find_joyport(app, e.jbutton.which, &port) == 1) {
						app->input.last_vk =
						(dhex_vk){
							.type = DHEX_INPUT_HW_JOYBUTTON,
							.hw.joybutton = {
								.button = e.jbutton.button,
								.device = port
							}
						};
						app->input.any_vk = 1;
					}
				}
				break;

				case SDL_JOYAXISMOTION:
				{
					uint8_t port;
					if (local_dhex_find_joyport(app, e.jaxis.which, &port) == 1 && abs(e.jaxis.value) > app->input.axis_deadzone) {
						
						signed char axis_dir = 1;
						if (e.jaxis.value < 0) axis_dir = -1;
						app->input.last_vk =
						(dhex_vk) {
							.type = DHEX_INPUT_HW_JOYAXIS,
							.hw.joyaxis = {
								.axis = e.jaxis.axis,
								.device = port,
								.direction = axis_dir
							}
						};
						
						app->input.any_vk = 1;
					}
				}
				break;

				case SDL_JOYHATMOTION:
				{
					uint8_t port;
					uint8_t dir = e.jhat.value;
					
					switch(dir) {
						
						case SDL_HAT_LEFTUP:    dir = SDL_HAT_UP;   break;
						case SDL_HAT_RIGHTUP:   dir = SDL_HAT_UP;   break;
						case SDL_HAT_LEFTDOWN:  dir = SDL_HAT_DOWN; break;
						case SDL_HAT_RIGHTDOWN: dir = SDL_HAT_DOWN; break;
						
						default: break;
					}
					
					if (local_dhex_find_joyport(app, e.jhat.which, &port) && (e.jhat.value != SDL_HAT_CENTERED)) {
						app->input.last_vk =
						(dhex_vk) {
							DHEX_INPUT_HW_JOYHAT,
							.hw.joyhat = {
								.hat = e.jhat.hat,
								.device = port,
								.direction = dir
							}
						};
						
						app->input.any_vk = 1;
					}
				}
				break;
				
				
			}
		
	}
	
	for (int player = 0; player < app->input.max_players; player++)
	for (int btn = 0; btn < DHEX_INPUT_MAX; btn++) {
		app->input.state_prev[btn][player] = app->input.state[btn][player];
		app->input.state[btn][player] = dhex_check_vk_direct(app, app->input.config[btn][player] );
	}
	
	int raw_mx, raw_my;
	SDL_GetMouseState(&raw_mx, &raw_my);
	
	int win_x, win_y;
	SDL_GetWindowSize( ((_dhex_app_private*)app->private)->window, &win_x, &win_y);
	
	int virtual_x, virtual_y;
	virtual_x = app->main_canvas->pixelbuffer.w;
	virtual_y = app->main_canvas->pixelbuffer.h;
	
	if (app->scaler == DHEX_SCALER_STRETCH) {
		
		double ptg_x = (double)(raw_mx) / (double)(win_x);
		app->input.mouse_x = (int) ((double)(virtual_x) * ptg_x);
				
		double ptg_y = (double)(raw_my) / (double)(win_y);
		app->input.mouse_y = (int) ((double)(virtual_y) * ptg_y);			
	}
	else {
		
		double win_ratio = (double)win_x / (double)win_y;
		double virtual_ratio;
		
		if (app->ratio_correction_enabled == 0) 
			virtual_ratio = (double)virtual_x / (double)virtual_y;
		else
			virtual_ratio = app->aspect_ratio;//(double)virtual_x / (double)virtual_y;
			
		int blackbar_x = 0; //in pixels
		int blackbar_y = 0;
		
		if (win_ratio > virtual_ratio) { //then blackbars are horizontal
			int screen_xport = win_y * virtual_ratio;
			blackbar_x = (win_x - screen_xport) / 2;
		}
		else {
			int screen_yport = win_x / virtual_ratio;
			blackbar_y = (win_y - screen_yport) / 2;			
		}
		
		double ptg_x = (double)iclamp(raw_mx - blackbar_x, 0, win_x - blackbar_x) / (double)(win_x - 2*blackbar_x);
		double ptg_y = (double)iclamp(raw_my - blackbar_y, 0, win_y - blackbar_y) / (double)(win_y - 2*blackbar_y);
		
		app->input.mouse_x = (int) ((double)(virtual_x) * ptg_x);	
		app->input.mouse_y = (int) ((double)(virtual_y) * ptg_y);	
	}
	
	
}
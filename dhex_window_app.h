/*
	be sure to include dhex_draw.h and link against dhex_draw.c somehow.
	SDL2 and SDL_mixer are dependencies for dhex_window_app.c 
	
	Usage example:
	int main () {
		//Initialize some resources here...
		
		dhex_app* app = dhex_create_app("My app", SCREEN_W, SCREEN_H, 4.0/3.0, 1, 0);
		
		while (app->quit == 0) {
			dhex_refresh_app(app);
			
			//do some game logic and dhex_draw.h draw calls here.
			//use app->main_canvas as your target canvas for the resulting frame on the window 
			
			
			dhex_render_canvas(app->main_canvas); 
			dhex_refresh_window(app); 
			dhex_microsleep(MICROS_PER_FRAME);
		}
		
		dhex_free_app(app);
		return 0;
	
	}

*/



#ifndef __dhex_window_app
#define __dhex_window_app

#define __DHEX_MAX_DEVICES 8
#define DHEX_NULL_DEVICE 0xFFFFFFFF

#define DHEX_SCALER_RATIO 0
#define DHEX_SCALER_STRETCH 1

//default frequency is 22050 samples per second
//default format is signed 16 bit
//2 channels (stereo, nothing to do with mixing channels)
//so, bytes per second of sfx = 22050 * 2 (bytes per sample) * 2 (channels)
//so bps = 88200
//bytes per frame (at 60hz) = 1470
	
#define DHEX_SFX_BYTES_PER_SECOND 88200
#define DHEX_SFX_CHANNELS 8

//samples per second
#define DHEX_SFX_FREQUENCY 22050

#define DHEX_SFX_CHUNKSIZE 1024

#include "dhex_draw.h"

typedef struct {
	int64_t seconds; //elapsed since epoch
	int32_t microseconds; //elapsed since <seconds>
} dhex_timestamp;

//extern dhex_timestamp _DHEX_PROGRAM_EPOCH_START; //initialized each time dhex_create_app gets called 

enum _DHEX_INPUT_HW_TYPE {
	DHEX_INPUT_HW_NONE = 0,
	DHEX_INPUT_HW_KEYBOARD,
	DHEX_INPUT_HW_JOYBUTTON,
	DHEX_INPUT_HW_JOYAXIS,
	DHEX_INPUT_HW_JOYHAT
	
};

enum _DHEX_INPUTS {
	DHEX_INPUT_0 = 0,
	DHEX_INPUT_1,
	DHEX_INPUT_2,
	DHEX_INPUT_3,
	DHEX_INPUT_4,
	DHEX_INPUT_5,
	DHEX_INPUT_6,
	DHEX_INPUT_7,
	DHEX_INPUT_8,
	DHEX_INPUT_9,
	DHEX_INPUT_10,
	DHEX_INPUT_11,
	DHEX_INPUT_12,
	DHEX_INPUT_13,
	DHEX_INPUT_14,
	DHEX_INPUT_15,
	DHEX_INPUT_MAX
};

typedef struct {
	char type; //one of _DHEX_INPUT_HW_TYPE macros
	union {
		struct {
			int16_t key;
		} keyboard;
		
		struct {
			int16_t button; //button number
			int8_t device; //index for device port table
		} joybutton;
		
		struct {
			int16_t axis;
			int8_t device; //index for device port table
			signed char direction; //1 for right, -1 for left			
		} joyaxis;
		
		struct {
			uint8_t hat;
			int8_t device;
			uint8_t direction;			
		} joyhat;
		
	} hw;
} dhex_vk;



typedef struct {
	dhex_canvas* main_canvas;
	char quit;
	char scaler; //ratio or stretch
	char clear_window_queued; //char bool. If set to true, next time dhex_refresh_window gets called
	//the window's surface (not dhex canvas) will get cleared to black.
	char hide_out; //if true no printf is executed 
	
	double aspect_ratio; //to account for games with non square pixels 
	char ratio_correction_enabled;
	
	struct {
		char any_vk; //0 false, 1 true. Is any vk found in this input refresh?
		dhex_vk last_vk; //last vk found
		int16_t axis_deadzone; //the larger, the more you have to move the analog
		//set to a positive value pwease
		
		int max_players;
		dhex_vk* config[DHEX_INPUT_MAX]; //2D array. the other dimension is the # of players
		//access with config[button][player]
		
		//these two are accessed the same as config. int bools. they store
		//wheter or not an input (dhex inputs, 0 to 15 supported)
		//is held this frame or was held the previous one
		int* state[DHEX_INPUT_MAX];
		int* state_prev[DHEX_INPUT_MAX];
		
		int mouse_x, mouse_y; //mouse coords in virtual space
		char mouse_held, mouse_pressed; //good ole char bool

	} input;
	
	void* private; //pointer to a struct that holds implementation specific stuff
	
} dhex_app;


enum _DHEX_CREATE_APP_FLAGS {
		DHEX_APP_SFX = 0b00000001,
		DHEX_APP_ASPECT_RATIO_CORRECTION = 0b00000010,
		DHEX_APP_HIDE_STDOUT = 0b00000100
};

dhex_app* dhex_create_app(const char* display_name, int w, int h, double aspect_ratio, int max_players, unsigned char flags);
void dhex_free_app(dhex_app* app);
void dhex_refresh_window(dhex_app* app);
void dhex_refresh_app(dhex_app* app);

void dhex_set_window_size(dhex_app* app, int w, int h);
void dhex_set_scaler(dhex_app* app, char scaler);

int64_t dhex_get_micros(void); //microseconds elapsed since app start
void    dhex_microsleep(int64_t micros);


//these two functions work together with a hidden variable in dhex_window_app.c
//dhex_set_timer() will store a timestamp of when it's called.
//dhex_wait_timer(micros) will block the execution until <micros> microseconds have elapsed 
//	since the recorded timestamp.
//IF USED BEFORE dhex_create_app(..) IT WILL BE CONSIDERED UNDEFINED BEHAVIOUR
void dhex_set_timer();
void dhex_wait_timer(int64_t micros);

int dhex_check_vk_direct(dhex_app* app, dhex_vk vk); //returns 0 on false
int dhex_input_check(dhex_app* app, int input, int player);
void dhex_input_config_set(dhex_app* app, int button, int player, dhex_vk vk); //set button config in ram
void dhex_input_config_save(dhex_app* app, const char* filename); //save and load full config from/to file
void dhex_input_config_load(dhex_app* app, const char* filename);

//utilizing these functions without previously creating a dhex_app with the DHEX_APP_SFX flag
//is considered undefined behaviour for the scope of this library.
//for both music and sfx only ogg format is supported
void* dhex_load_sfx(const char* filename); //returns void* cuz struct is implementation specific
void  dhex_free_sfx(void* sfx); //for the love of god make sure to send the right data to this
void* dhex_load_music(const char* filename);
void  dhex_free_music(void* music);


//these are literally wrappers for SDL_mixer functions:

int dhex_sfx_volume(void* sfx, int volume); //int from 0 to 128. if less than 0, volume won't be set
//Returns previous sfx volume setting.

int dhex_sfx_channel_volume(int channel, int volume); //channel from 0 to 7, volume from 0 to 128. if less than 0, volume won't be set
//-1 will set the volume for all allocated channels. 
//Returns current volume of the channel. If channel is -1, the average volume is returned. 

int dhex_music_volume(int volume); //0 to 128. -1 will not set anything
//Returns the previous volume setting

int dhex_music_play(void *music, int loops); //loops is how many times the music should play. -1 sets endless loops 
//Returns 0 on success, or -1 on errors. 

void dhex_music_pause(void);
void dhex_music_resume(void);
void dhex_music_halt(void);

void dhex_sfx_play(int channel, void* sfx, int64_t skip_bytes); //pass -1 to channel to use the first available channel found.
//skip_bytes are the number of bytes of raw data to be skipped from the beginning of the audio. Used mostly for rollback netcode
//stuff (so that audio changes can be properly rolledback)
//use DHEX_SFX_BYTES_PER_SECOND to define how much you want to skip 

void dhex_sfx_halt_channel(int channel); //pass -1 to stop all channels

#endif
  DHEX: Dynamic Hitline EX libraries. A collection of C libraries and tools designed to make 2D fighting games.

  This software is composed of the following files in the parent directory:
	clist.c, clist.h,
	dhex_bxg.c, dhex_bxg.h,
	dhex_draw.c, dhex_draw.c, 
	dhex_font.c, dhex_font.h,
	dhex_rollback.c, dhex_rollback.h,
	dhex_window_app.c, dhex_window_app.h, dhex_window_app_keycodes.h,
	palgen.c,
	boxgen.c,
	resource_manager.c, resource_manager.h

  Copyright (C) 2022 Mateo Flórez Bacca

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

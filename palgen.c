/*
	PALGEN - Palette generator for dhex 
	
	Usage:
	
	There are 3 modes. Every mode is meant to receive PNG files as input.
	
	palgen -auto <files...>
		Generates palettized versions of <files...>. Palette file is automatically generated
		
	palgen -plt <pal> <files...>
		Generates palettized versions of <files...> using an already existing palette.
		If more colors are found and the palette has space, the extra colors will be added
		to the original palette file
		
	palgen -gui <spr> <pal>
		Loads a GUI that allows to create custom palettes. Displays <spr> (a palettized PNG)
		colorized with <pal>. Once the desired custom palette has been created, it can be 
		exported as custom.pal
*/

#include "dhex_window_app.h"
#include "dhex_font.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600
#define MICROS_PER_FRAME 36000
#define FILE_AUTO_PAL "auto.pal"
#define FILE_CUSTOM_PAL "custom.pal"
#define FILE_CUSTOM_PNG "custom.png"
#define SCALE_MIN 1.0
#define SCALE_MAX 8.0
#define SPR_SCREENPOS_X SCREEN_WIDTH/4
#define SPR_SCREENPOS_Y SCREEN_HEIGHT/2

void notice_usage() {
	printf(
	"\n"
	"PALGEN - Palette generator for dhex "
	"\n\n"
	"Usage:"
	"\n\n"
	"There are 3 modes. Every mode is meant to receive PNG files as input."
	"\n\n"
	"palgen -auto <files...>" "\n"
	"\tGenerates palettized versions of <files...>. Palette file is automatically generated"
	"\n\n"
	"palgen -plt <pal> <files...>" "\n"
	"\tGenerates palettized versions of <files...> using an already existing palette." "\n"
	"\tIf more colors are found and the palette has space, the extra colors will be added" "\n"
	"\tto the original palette file"
	"\n\n"
	"palgen -gui <spr> <pal>" "\n"
	"\tLoads a GUI that allows to create custom palettes. Displays <spr> (a palettized PNG)" "\n"
	"\tcolorized with <pal>. Once the desired custom palette has been created, it can be " "\n"
	"\texported as " FILE_CUSTOM_PAL
	"\n\n"
	
	);
}

static inline int64_t iclamp(int64_t val, int64_t min, int64_t max) {
	if (val < min) return min;
	if (val > max) return max;
	return val;
}

static inline int mouse_in(dhex_app* app, int x, int y, int w, int h) {
	int mx = app->input.mouse_x;
	int my = app->input.mouse_y;
	
	if (
		mx >= x && mx <= x+w &&
		my >= y && my <= y+h	
	) {
		return 1;
	}
	
	return 0;
}

int maingui(int argc, char* argv[]) {

	//Initialize some resources here...
	dhex_font_init();
	dhex_palette pal_txt;
	pal_txt.ncolors = 1;
	pal_txt.colors[0] = (dhex_rgba){0,0,0,0};
	pal_txt.colors[1] = (dhex_rgba){0xFF,0xFF,0xFF,0xFF};
	
	dhex_sprite* spr = dhex_load_sprite_png(argv[2]);
	if (spr == NULL) {
		printf("Error: couldn't load %s\n", argv[2] );
		return -1;
	}
	
	int w,h;
	w = spr->w;
	h = spr->h;
	
	int xcenter, ycenter;
	xcenter = w/2;
	ycenter = h/2;
	
	dhex_palette* pal = dhex_load_palette(argv[3]);
	if (pal == NULL) {
		printf("Error: couldn't load %s\n", argv[3] );
		return -1;
	}
	
	dhex_palette support_palette;
	memcpy(&support_palette, pal, sizeof(dhex_palette));
	
	dhex_palette undo_palette;
	memcpy(&undo_palette, pal, sizeof(dhex_palette));
	
	u8 selected_index = 0;
	
	char canApply = 0;
	char canUndo  = 0;
	char canExportPal = 1;
	char canExportPng = 1;
	
	u8 background_brightness = 255;
	
	dhex_app* app = dhex_create_app("PALGEN V2", SCREEN_WIDTH, SCREEN_HEIGHT, 4.0d/3.0d, 1, 0);
	
	float scale = 3.0;
	
	while (app->quit == 0) {
		
		int64_t tStart = dhex_get_micros();
		
		dhex_refresh_app(app);
		
		dhex_set_colormode(DHEX_COLORMODE_SET, app->main_canvas);
		
		//background ////////////////////////////////////////////////
		dhex_draw_rect( (dhex_rgba){background_brightness,background_brightness,background_brightness,255},
			app->main_canvas, 0, 0, SCREEN_WIDTH/2, SCREEN_HEIGHT);
		
		//edited sprite ////////////////////////////////////////////////
		dhex_draw_sprite_ex(spr, &support_palette, app->main_canvas, SPR_SCREENPOS_X, SPR_SCREENPOS_Y,
		xcenter, ycenter, scale, scale);
		
		//grey wall ////////////////////////////////////////////////
		dhex_draw_rect( (dhex_rgba){90,90,90,255}, app->main_canvas, SCREEN_WIDTH/2, 0, SCREEN_WIDTH/2, SCREEN_HEIGHT);
		
		//palette index selector ////////////////////////////////////////////////
		for (int k = 0; k <= pal->ncolors; k++) {
			int xStart = SCREEN_WIDTH/2 + 10;
			int yStart = 10;
			
			int size = 18;
			int offset = size + 4;
			
			int xpos = k % 16;
			int ypos = k / 16;
			
			dhex_rgba color = (dhex_rgba){255,255,255,255};
			
			if (selected_index == k) color = (dhex_rgba){255,0,0,255};
			
			dhex_draw_rect(color, app->main_canvas, 2+xStart + offset*xpos, 2+yStart + ypos*offset,
				size, size);
				
			color = pal->colors[k];
			
			dhex_draw_rect(color, app->main_canvas, xStart + offset*xpos, yStart + ypos*offset,
				size, size);			
			
			if (mouse_in(app, xStart + offset*xpos, yStart + ypos*offset,size, size) ) {
				if (app->input.mouse_pressed) {
					selected_index = k;
					memcpy(&support_palette, pal, sizeof(dhex_palette));
					canApply = 0;
				}
			}
			
		}
		
		dhex_rgba* current_color = &support_palette.colors[selected_index];
		
		//rgba sliders ////////////////////////////////////////////////
		if (selected_index > 0)
		for (int k = 0; k < 4; k++) {
			int xStart = SCREEN_WIDTH/2 + 30;
			int yStart = SCREEN_HEIGHT/2 + 72;
			int vOffset = 28;
			
			char comp[7];
			u8* comp_addr = NULL;
			
			switch(k) {
				case 0: sprintf(comp, "R %d\0", current_color->r); comp_addr = &current_color->r; break;
				case 1: sprintf(comp, "G %d\0", current_color->g); comp_addr = &current_color->g; break;
				case 2: sprintf(comp, "B %d\0", current_color->b); comp_addr = &current_color->b; break;
				case 3: sprintf(comp, "A %d\0", current_color->a); comp_addr = &current_color->a; break;
			}
			
			dhex_font_draw_text(&DHEX_FONT_DEFAULT, &pal_txt, app->main_canvas, comp, DHEX_FONT_ALIGN_LEFT,
			xStart, yStart + vOffset*k, 2.0, 2.0);
			
			for (int button = 0; button < 4; button++) {
				
				int btn_w = 40;
				int btn_h = 24;
				int xStart2 = xStart + 140 + (btn_w + 8) * button - btn_w/2;
				int yStart2 = yStart + vOffset*k;
				
				dhex_draw_rect((dhex_rgba){0,0,0,255}, app->main_canvas, xStart2, yStart2, btn_w, btn_h);
				
				if (mouse_in(app, xStart2, yStart2, btn_w, btn_h) ) {
					
					int slide_speed = 6;
					
					switch(button) {
						case 0:
						{
							if (app->input.mouse_held) {
								if (*comp_addr > 0) {
									*comp_addr = iclamp((int)*comp_addr - slide_speed, 0, 255);
									canApply = 1;
									canExportPal = 1;
									canExportPng = 1;
								}
								
							}
						}
						break;
						
						case 1:
						{
							if (app->input.mouse_held) {
								if (*comp_addr < 255) {
									*comp_addr = iclamp((int)*comp_addr + slide_speed, 0, 255);
									canApply = 1;
									canExportPal = 1;
									canExportPng = 1;
								}
							}
						}
						break;
						
						
						case 2:
						{
							if (app->input.mouse_pressed) {
								if (*comp_addr > 0) {
									*comp_addr = iclamp((int)*comp_addr - 1, 0, 255);
									canApply = 1;
									canExportPal = 1;
									canExportPng = 1;									
								}
							}
						}
						break;
						
						case 3:
						{
							if (app->input.mouse_pressed) {
								if (*comp_addr < 255) {
									*comp_addr = iclamp((int)*comp_addr + 1, 0, 255);
									canApply = 1;
									canExportPal = 1;
									canExportPng = 1;
								}
							}
						}
						break;
					}					
					
				}
				
				char caption[9];
				switch(button) {
					case 0: sprintf(caption, "%%l%%l\0"); break;
					case 1: sprintf(caption, "%%r%%r\0"); break;
					case 2: sprintf(caption, "%%l\0"); break;
					case 3: sprintf(caption, "%%r\0"); break;
				}
				
				dhex_font_draw_text(&DHEX_FONT_DEFAULT, &pal_txt, app->main_canvas, caption, DHEX_FONT_ALIGN_CENTERED,
				xStart2+btn_w/2, yStart2+btn_h/2, 2.0, 2.0);				
				
			}
		}
		
		//apply and undo buttons ////////////////////////////////////////////////
		for (int k = 0; k < 2; k++) {
			
			int btn_w = 100;
			int btn_h = 26;
			int xStart = SCREEN_WIDTH/2 + 70 + (btn_w + 7)*k - btn_w/2;
			int yStart = 506 - btn_h/2;
			
			char canDraw = 0;
			if ((k == 0 && canApply) || (k == 1 && canUndo)) canDraw = 1;
			
			char caption[9];
			if (k == 0) 
				sprintf(caption, "apply\0");
			else 
				sprintf(caption, "undo\0");
			
			if (canDraw) {	
				dhex_draw_rect((dhex_rgba){0,0,0,255}, app->main_canvas, xStart, yStart, btn_w, btn_h);
				dhex_font_draw_text(&DHEX_FONT_DEFAULT, &pal_txt, app->main_canvas, caption, DHEX_FONT_ALIGN_CENTERED,
				xStart+btn_w/2, yStart+btn_h/2, 2.0, 2.0);	
				
				if (mouse_in(app, xStart, yStart, btn_w, btn_h)) {
					
					if (app->input.mouse_pressed) {
						
						if (k == 0) { //aka pressed the apply button 
							memcpy(&undo_palette, pal, sizeof(dhex_palette));
							memcpy(pal, &support_palette, sizeof(dhex_palette));
							canApply = 0;
							canUndo = 1;
						}
						else {
							memcpy(pal, &undo_palette, sizeof(dhex_palette));
							memcpy(&support_palette, &undo_palette, sizeof(dhex_palette));
							canApply = 0;
							canUndo = 0;							
							
						}
						
					}
				}
			}
		}
		
		
		
		//export pal and image buttons  ////////////////////////////////////////////////
		for (int k = 0; k < 2; k++) {
			
			int btn_w = 250;
			int btn_h = 26;
			int xStart = SCREEN_WIDTH/2 + 140 - btn_w/2;
			int yStart = 540 - btn_h/2 + (btn_h + 6)*k;
			
			char canDraw = 0;
			if ((k == 0 && canExportPal) || (k == 1 && canExportPng)) canDraw = 1;
			
			char caption[20];
			if (k == 0) 
				sprintf(caption, "export pal\0");
			else 
				sprintf(caption, "export png\0");
			
			if (canDraw) {	
				dhex_draw_rect((dhex_rgba){0,0,0,255}, app->main_canvas, xStart, yStart, btn_w, btn_h);
				dhex_font_draw_text(&DHEX_FONT_DEFAULT, &pal_txt, app->main_canvas, caption, DHEX_FONT_ALIGN_CENTERED,
				xStart+btn_w/2, yStart+btn_h/2, 2.0, 2.0);	
				
				if (mouse_in(app, xStart, yStart, btn_w, btn_h)) {
					
					if (app->input.mouse_pressed) {
						
						if (k == 0) { //aka pressed the export pal button 
							canExportPal = 0;
							dhex_save_palette(&support_palette, FILE_CUSTOM_PAL); //palgen v2 file
						}
						else {
							canExportPng = 0;
							dhex_canvas* spcv = dhex_create_canvas(spr->w, spr->h);
							dhex_set_colormode(DHEX_COLORMODE_SET, spcv);
							dhex_clear_canvas(spcv);
							dhex_draw_sprite(spr, &support_palette, spcv, 0,0, 0,0);
							dhex_set_colormode(DHEX_COLORMODE_SET, spcv);
							dhex_render_canvas(spcv);
							dhex_dump_image_png(FILE_CUSTOM_PNG, &spcv->pixelbuffer);
							dhex_free_canvas(spcv);
						}
						
					}
				}
			}
		}
		
		char CanSelectIndexFromSprite = 1;
		
		// background color slider //////////////////////////////////////////
		for (int k = 0; k < 2; k++) {
			int btn_w = 40;
			int btn_h = 24;
			int xStart = 30 + (btn_w + 8) * k - btn_w/2;
			int yStart = 26;	
			
			char canDraw = 0;
			if ( (k == 0 && background_brightness > 0) || (k == 1 && background_brightness < 255) ) {
				canDraw = 1;
			}
			
			if (canDraw) {
				dhex_draw_rect((dhex_rgba){0,0,0,255}, app->main_canvas, xStart, yStart, btn_w, btn_h);
				
				char caption[5];
				if (k == 0) 
					sprintf(caption, "%%l\0");
				else 
					sprintf(caption, "%%r\0");
				
				dhex_font_draw_text(&DHEX_FONT_DEFAULT, &pal_txt, app->main_canvas, caption, DHEX_FONT_ALIGN_CENTERED,
				xStart+btn_w/2, yStart+btn_h/2, 2.0, 2.0);	
				
				if (mouse_in(app, xStart, yStart, btn_w, btn_h) ) {
					if (app->input.mouse_held) {
						CanSelectIndexFromSprite = 0;
						if (k == 0) {
							background_brightness = iclamp((int)background_brightness - 4, 0, 255);
						}
						else {
							background_brightness = iclamp((int)background_brightness + 4, 0, 255);
						}
					}
					
				}
			}
		}
		
		// scale slider //////////////////////////////////////////
		for (int k = 0; k < 2; k++) {
			int btn_w = 24;
			int btn_h = 40;
			int xStart = SCREEN_WIDTH/2 - 50;
			int yStart = SCREEN_HEIGHT - 100 + (btn_h + 8) * k;	
			
			char canDraw = 0;
			if ( (k == 0 && scale < SCALE_MAX) || (k == 1 && scale > SCALE_MIN) ) {
				canDraw = 1;
			}
			
			if (canDraw) {
				dhex_draw_rect((dhex_rgba){0,0,0,255}, app->main_canvas, xStart, yStart, btn_w, btn_h);
				
				char caption[5];
				if (k == 0) 
					sprintf(caption, "%%u\0");
				else 
					sprintf(caption, "%%d\0");
				
				dhex_font_draw_text(&DHEX_FONT_DEFAULT, &pal_txt, app->main_canvas, caption, DHEX_FONT_ALIGN_CENTERED,
				xStart+btn_w/2, yStart+btn_h/2, 2.0, 2.0);	
				
				if (mouse_in(app, xStart, yStart, btn_w, btn_h) ) {
					if (app->input.mouse_held) {
						CanSelectIndexFromSprite = 0;
						if (k == 0) {
							scale = fmin(scale + 0.15, SCALE_MAX);
						}
						else {
							scale = fmax(scale - 0.15, SCALE_MIN);
						}
					}
					
				}
			}
		}
		
		//sprite x/ycenter sliders //////////////////////////////////////////
		{
			int xStart = 690;
			int yStart = 490;
			int size = 100;
			
			dhex_draw_rect((dhex_rgba){255,255,255,255}, app->main_canvas, xStart, yStart, size, size);
			
			if (mouse_in(app, xStart, yStart, size, size)) {
				if (app->input.mouse_held) {
					float xx = (float)(app->input.mouse_x - xStart) / (float)size;
					float yy = (float)(app->input.mouse_y - yStart) / (float)size;
					
					xcenter = w * xx;
					ycenter = h * yy;
				}
			}
			
		}
		
		//index selector from sprite //////////////////////////////////////////
		if (CanSelectIndexFromSprite)
		{
			int spr_w = w * scale;
			int spr_h = h * scale;
			
			int spr_xstart = SPR_SCREENPOS_X - xcenter * scale;
			int spr_ystart = SPR_SCREENPOS_Y - ycenter * scale;
			
			int sp_spr_w;
			sp_spr_w = iclamp(spr_w - ( spr_xstart + spr_w - SCREEN_WIDTH/2 ), 0, spr_w);
			
			if (mouse_in(app, spr_xstart, spr_ystart, sp_spr_w, spr_h)) {
				if (app->input.mouse_pressed) {
					float xx = (float)(app->input.mouse_x - spr_xstart) / (float)spr_w;
					float yy = (float)(app->input.mouse_y - spr_ystart) / (float)spr_h;

					int xpixel = iclamp(w * xx, 0, w - 1);
					int ypixel = iclamp(h * yy, 0, h - 1);
					
					
					selected_index = spr->pixeldata[xpixel + w*ypixel];
					memcpy(&support_palette, pal, sizeof(dhex_palette));
					canApply = 0;
				}
			}
			
		}
		
		dhex_refresh_window(app); //this will internally call dhex_render_canvas() 
		
		int64_t tEnd = dhex_get_micros();
	
		dhex_microsleep(iclamp(MICROS_PER_FRAME - (tEnd - tStart), 0, MICROS_PER_FRAME));
	}
	
	dhex_free_app(app);
	return 0;
}

//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////

enum MODES {
	MODE_NONE = 0,
	MODE_AUTO,
	MODE_PLT,
	MODE_GUI
};

int main(int argc, char* argv[]) {
	
	if (argc < 3) {
		notice_usage();
		return -1;
	}
	
	char mode = MODE_NONE;
	
	if (strcmp(argv[1], "-auto") == 0) { mode = MODE_AUTO; }
	if (strcmp(argv[1], "-plt" ) == 0) { mode = MODE_PLT;  }
	if (strcmp(argv[1], "-gui" ) == 0) { mode = MODE_GUI;  }
	
	if (mode == MODE_NONE) {
		notice_usage();
		return -1;		
	}
	
	if (mode == MODE_GUI) {
		if (argc == 4)
			return maingui(argc, argv);
		else {
			notice_usage();
			return -1;
		}
	}
	
	dhex_palette pal;
	pal.ncolors = 0;
	pal.colors[0] = (dhex_rgba){0,0,0,0};
	
	int nimages = argc - 2; //number of input images
	int img_start = 2; //from what argv index the input images start 
	
	if (mode == MODE_PLT) {
		dhex_palette* spal = dhex_load_palette(argv[2]); //palgen v2 file
		
		if (spal == NULL) {
			printf("Error: couldn't load %s\n", argv[2] );
			return -1;
		}	
		
		pal = *spal;
		free(spal);
		nimages = argc - 3;
		img_start = 3;
	}
	
	//for each image 
	for (int k = 0; k < nimages; k++) {
		
		dhex_image* input_img = dhex_load_image_png( argv[img_start + k] );
		
		if (input_img == NULL) {
			printf("Warning: couldn't load %s\n", argv[img_start + k] );
		}
		else {
			int w,h;
			w = input_img->w;
			h = input_img->h;
			//palettized representation of the image will be saved here 
			dhex_image support_img;
			support_img.w = w;
			support_img.h = h;

			support_img.pixeldata = malloc(sizeof(dhex_rgba) * w * h);
			
			//rgba representation of the palettized image will be saved here 
			dhex_canvas* support_canvas = dhex_create_canvas(w, h);
			
			//for each pixel
			for (int py = 0; py < h; py++)
			for (int px = 0; px < w; px++) {
				
				dhex_rgba color = input_img->pixeldata[px + w*py];
				u8 c_index = 0;
				char found_index = 0;
				
				//for each palette index
				for (int c = 1; c <= pal.ncolors; c++) {
					dhex_rgba pcolor = pal.colors[c];
					
					if (
						color.r == pcolor.r &&
						color.g == pcolor.g &&
						color.b == pcolor.b &&
						color.a == pcolor.a 
					)
					{
						found_index = 1;
						c_index = (u8)c;
						break;
					}
				}
				
				if 
				(
					(
					color.r == 0 &&
					color.g == 255 &&
					color.b == 255 &&
					color.a == 255 
					)
					||
					(
					//color.r == 0 &&
					//color.g == 0 &&
					//color.b == 0 &&
					color.a == 0 
					)
				) 
				
				{ //special case 
					found_index = 1;
					c_index = 0;					
				}
				
				if (found_index == 0 && pal.ncolors < 255) {
					pal.ncolors++;
					c_index = pal.ncolors;
					pal.colors[pal.ncolors] = color;
				}
				
				u8 b = 0;
				if (c_index == 0) b = 255;
				support_img.pixeldata[px + w*py] = (dhex_rgba){c_index, 255, b, 255};
			}
			
			//now, the palettized image is represented in support_img
			
			dhex_set_colormode(DHEX_COLORMODE_SET, support_canvas);
			dhex_clear_canvas(support_canvas);
			dhex_draw_image(&support_img, support_canvas, 0,0, 0,0);
			dhex_render_canvas(support_canvas);
			
			int fileInputLen = strlen(argv[img_start + k]);
			char* fileOutput = malloc(fileInputLen + 5); //4 chars for pal_ and 1 extra for null terminating
			sprintf(fileOutput, "pal_%s\0", argv[img_start + k]);
			dhex_dump_image_png(fileOutput, &(support_canvas->pixelbuffer) );
			
			//cleanup
			free(fileOutput);
			free(support_img.pixeldata);
			dhex_free_image(input_img);
			dhex_free_canvas(support_canvas);
			
		}
		
	}
	
	const char* palFileOutput;
	
	if (mode == MODE_AUTO) {
		palFileOutput = FILE_AUTO_PAL;
	}
	else {
		palFileOutput = argv[2];
	}
	
	dhex_save_palette(&pal, palFileOutput); //palgen v2 file
	
	return 0;
}
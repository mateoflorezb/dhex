#include "resource_manager.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

typedef struct {
	void* data;
	char* filename;
	int refcount;
} _rman_node;

resource_manager* resource_manager_create(void* (*rload)(const char*), void (*rfree)(void*)) {
	resource_manager* rman = malloc(sizeof(resource_manager));
	rman->resources = clist_create(_rman_node);
	rman->rload = rload;
	rman->rfree = rfree;
	
	return rman;
}

void* resource_manager_getref(resource_manager* rman, const char* filename) {

	//check if it already exists
	clist_iterator it = clist_start(rman->resources);
	while (it != NULL) {
			
		#define nd (clist_get(it, _rman_node))

		if (strcmp(filename, (const char*)nd.filename) == 0) {
			nd.refcount++;
			//printf("Got refcount=%d\n", nd.refcount);
			return nd.data;
		}
		
		#undef nd
		
		it = clist_next(it);
	}
	
	void* new_rs = rman->rload(filename);
	if (new_rs == NULL) return NULL;
	
	_rman_node nd;
	nd.data = new_rs;
	char* s = malloc(strlen(filename) + 1);
	strcpy(s, filename);
	nd.filename = s;
	nd.refcount = 1;
	clist_push(rman->resources, &nd);
	return new_rs;
}

void resource_manager_decref(resource_manager* rman, void* rs) {
	
	clist_iterator it = clist_start(rman->resources);
	while (it != NULL) {
		
		clist_iterator next = clist_next(it);
			
		#define nd (clist_get(it, _rman_node))

		if (rs == nd.data) {
			nd.refcount--;
			//printf("Got refcount=%d  (dec)\n", nd.refcount);
			if (nd.refcount <= 0) {
				rman->rfree(nd.data);
				free(nd.filename);
				clist_delete(it);
			}
			return;
		}
		
		#undef nd
		
		it = next;
	}	
}

void resource_manager_flush(resource_manager* rman) {
	
	clist_iterator it = clist_start(rman->resources);
	
	while (it != NULL) {
		
		clist_iterator next = clist_next(it);
		
		_rman_node nd = clist_get(it, _rman_node);
		rman->rfree(nd.data);
		free(nd.filename);
		clist_delete(it);
		
		it = next;
	}
}

void resource_manager_free (resource_manager* rman) {
	
	resource_manager_flush(rman);
	clist_free(rman->resources);
	free(rman);
}

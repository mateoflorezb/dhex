/*
	Usage (recommended):
	
	////////////////////////////////////////////////////////////////////
	// Initialization: 
	
		static void* <your_resource_loader>(const char* filename) {
			...
		}
		
		static void <your_resource_freer>(void* resource) {
			...
		}	
		
		resource_manager* rman = resource_manager_create(<your_resource_loader>, <your_resource_freer>);	
	
	////////////////////////////////////////////////////////////////////	
	// Get a resource: 	
	
		YourResourceType* new_resource = (YourResourceType*)resource_manager_getref(rman, "myresource.res");
		
	////////////////////////////////////////////////////////////////////
	// Release a resource:
	
		resource_manager_decref(rman, new_resource);
	
	////////////////////////////////////////////////////////////////////
	// Free every resource:
	
		resource_manager_flush(rman);
		
	////////////////////////////////////////////////////////////////////
	// Free resource manager from RAM:
	
		resource_manager_free (rman);

*/


#ifndef _RESOURCE_MANAGER_H
#define _RESOURCE_MANAGER_H

#include "clist.h"

typedef struct {
	clist* resources;
	void* (*rload)(const char*); //resource load and resource free callbacks (ex: wrapper for dhex_load_image_png that returns void*)
	void  (*rfree)(void*);
} resource_manager;

resource_manager* resource_manager_create(void* (*rload)(const char*), void (*rfree)(void*));
void  resource_manager_flush (resource_manager* rman);
void  resource_manager_free  (resource_manager* rman);
void* resource_manager_getref(resource_manager* rman, const char* filename);
void  resource_manager_decref(resource_manager* rman, void* rs);

#endif